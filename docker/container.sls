{% set docker = pillar.get('docker', {}) %}
{% set images = docker.get('images', {}) %}
{% set containers = docker.get('containers', []) %}


{% for container in containers %}
{% set name = container['name'] %}
{% set config = container %}
{% set volumes = config.get('volumes', {}) %}
{% for volume, conf in volumes.items() %}

docker-volume-{{ name }}-{{ volume }}:
  file.directory:
    - name: {{ docker['volume_root'] }}/{{ name }}/{{ volume }}
    - makedirs: True
    - require:
      - file: docker-volume-root
{% endfor %}

docker-container-{{ name }}:
  dockerng.running:
    - image: {{ config['image'] }}
    - name: {{ name }}
    - restart_policy: always
    - watch:
      - dockerng: docker-image-{{ config['image'] }}
    - require:
      - dockerng: docker-image-{{ config['image'] }}
    {% if volumes %}
    {% for volume in volumes.keys() %}
      - docker-volume-{{ name }}-{{ volume }}
    {% endfor %}
    {% endif %}
    {% if 'environment' in config %}
    - environment:
      {% for key, value in config['environment'].items() %}
      - {{ key }}: "{{ value }}"
      {% endfor %}
    {% endif %}
    {% if volumes %}
    - binds:
    {% for volume, volume_conf in volumes.items() %}
      - {{ docker['volume_root'] }}/{{ name }}/{{ volume }}:{{ volume_conf['destination'] }}{% if volume_conf.get('read_only', False) %}:ro{%endif%}
    {% endfor %}
    {% endif %}
    {% if 'ports' in config %}
    - port_bindings:
      {% for proto, ports in config['ports'].items() %}
      {% for inner, outer in ports.items() %}
      - "{{inner}}:{{outer}}/{{proto}}"
      {% endfor %}
      {% endfor %}
    {% endif %}
    {% if 'links' in config %}
    - links:
      {% for link in config['links'] %}
      - "{{ link }}"
      {% endfor %}
    {% endif %}
{% endfor %}

