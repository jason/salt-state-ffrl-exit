{%- from 'docker/map.jinja' import docker with context %}

docker:
  pkgrepo.managed:
    - humanname: docker
    - name: {{ docker.apt_repo }}
    - file: /etc/apt/sources.list.d/docker.list
    - key_url: salt://docker/files/docker.gpg
    - require:
      - pkg: apt-transport-https
  pkg.installed:
    - pkgs:
      - docker-ce
      - python-docker
    - require:
      - pkgrepo: docker
  service.running:
    - enable: True
    - watch:
      - file: /etc/systemd/system/docker.service.d/override.conf
    - require:
      - pkg: docker

/etc/ferm/conf.d/40-docker.conf:
  file.managed:
    - user: root
    - group: root
    - mode: 644
    - require:
      - file: /etc/ferm/conf.d
    - contents: |
        domain (ip ip6) table (nat filter) chain DOCKER;

docker-volume-root:
  file.directory:
    - name: {{ docker.volume_root | default('/srv/docker') }}
    - makedirs: True
    - require:
      - pkg: docker

/etc/systemd/system/docker.service.d/override.conf:
  file.managed:
    - user: root
    - group: root
    - mode: 644
    - makedirs: True
    - contents: |
        [Service]
        # keep empty ExecStart= to flush previous entries
        ExecStart=
        ExecStart=/usr/bin/dockerd -H fd:// --experimental --storage-driver={{ docker.storagedriver | default('overlay2') }}
    - require:
      - pkg: docker

include:
  - apt.transport.https
  - apt.dependencies
  - docker.images
  - docker.container
