{% set docker = pillar.get('docker', {}) %}
{% set images = docker.get('images', {}) %}


{% for name, config  in images.items() %}
docker-image-{{ name }}{% if 'tag' in config %}:{{ config['tag'] }}{% endif %}:
  dockerng.image_present:
    - name: {{ name }}{% if 'tag' in config %}:{{ config['tag'] }}{% endif %}
    - force: true
    - watch:
      - pkg: docker
    - require:
      - pkg: docker
{% endfor %}
