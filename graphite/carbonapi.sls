include:
  - apt.transport.https

carbonapi:
  pkgrepo.managed:
    - humanname: carbonapi
    - name: deb https://packagecloud.io/go-graphite/autobuilds/debian/ stretch main
    - key_url: https://packagecloud.io/go-graphite/autobuilds/gpgkey
    - file: /etc/apt/sources.list.d/go-graphite_autobuilds.list
    - require_in:
      - pkg: carbonapi
  pkg.installed: []
  service.running:
    - enable: True
    - require:
      - file: /etc/carbonapi/carbonapi.yaml
      - file: /etc/default/carbonapi
      - file: /etc/systemd/system/carbonapi.service
    - watch:
      - file: /etc/carbonapi/carbonapi.yaml
      - file: /etc/default/carbonapi
      - file: /etc/systemd/system/carbonapi.service

/etc/carbonapi/carbonapi.yaml:
  file.managed:
    - source: salt://graphite/files/carbonapi/carbonapi.yaml
    - user: root
    - group: root
    - mode: 644
    - makedirs: True
    - require:
      - pkg: carbonapi

/etc/default/carbonapi:
  file.managed:
    - source: salt://graphite/files/carbonapi/carbonapi-default
    - user: root
    - group: root
    - mode: 644
    - require:
      - pkg: carbonapi

/etc/systemd/system/carbonapi.service:
  file.managed:
    - source: salt://graphite/files/carbonapi/carbonapi.service
    - user: root
    - group: root
    - mode: 644
    - require:
      - pkg: carbonapi
