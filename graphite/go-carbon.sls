include:
  - ferm
  - logrotate

go-carbon:
  pkg.installed:
    - sources:
      - go-carbon: https://github.com/lomik/go-carbon/releases/download/v0.11.0/go-carbon_0.11.0_amd64.deb
    - allow_updates: true
  service.running:
    - enable: True
    - name: go-carbon
    - require:
      - pkg: go-carbon
      - file: /etc/systemd/system/go-carbon.service
      - file: /etc/go-carbon/go-carbon.conf
      - file: /etc/go-carbon/storage-aggregation.conf
      - file: /etc/go-carbon/storage-schemas.conf
    - watch:
      - file: /etc/systemd/system/go-carbon.service
      - file: /etc/go-carbon/go-carbon.conf
      - file: /etc/go-carbon/storage-aggregation.conf
      - file: /etc/go-carbon/storage-schemas.conf

/etc/systemd/system/go-carbon.service:
  file.managed:
    - source: salt://graphite/files/go-carbon/go-carbon.service
    - user: root
    - group: root
    - mode: 644
    - require:
      - pkg: go-carbon

/etc/go-carbon/go-carbon.conf:
  file.managed:
    - source: salt://graphite/files/go-carbon/go-carbon.conf
    - user: root
    - group: root
    - mode: 644
    - require:
      - pkg: go-carbon

/etc/go-carbon/storage-aggregation.conf:
  file.managed:
    - source: salt://graphite/files/go-carbon/storage-aggregation.conf
    - user: root
    - group: root
    - mode: 644
    - require:
      - pkg: go-carbon

/etc/go-carbon/storage-schemas.conf:
  file.managed:
    - source: salt://graphite/files/go-carbon/storage-schemas.conf
    - user: root
    - group: root
    - mode: 644
    - require:
      - pkg: go-carbon

/etc/logrotate.d/go-carbon:
  file.managed:
    - source: salt://graphite/files/go-carbon/logrotate-go-carbon.conf
    - user: root
    - group: root
    - mode: 644
    - require:
      - pkg: go-carbon
      - pkg: logrotate

/etc/ferm/conf.d/40-go-carbon.conf:
  file.managed:
    - source: salt://graphite/files/go-carbon/go-carbon-ferm.conf.j2
    - template: jinja
    - user: root
    - group: root
    - mode: 644
    - require:
      - pkg: go-carbon
      - pkg: ferm
