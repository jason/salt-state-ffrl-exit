include:
  - apt.transport.https
  - apt.dependencies

knot-dns-deb:
  pkgrepo.managed:
    - humanname: knot-dns.cz/knot/
{%- if grains['osfinger'] == 'Debian-8' %}
    - name: deb https://deb.knot-dns.cz/knot/ jessie main
    - dist: jessie
{%- else %}
    - name: deb https://deb.knot-dns.cz/knot/ stretch main
    - dist: stretch
{%- endif %}
    - file: /etc/apt/sources.list.d/knot.list
    - clean_file: True
    - key_url: salt://apt/files/keys/knot.gpg
    - require:
      - pkg: python-apt
      - pkg: apt-transport-https

knot-common:
  pkg.installed:
    - pkgs:
      - knot-dnsutils
      - knot-host
