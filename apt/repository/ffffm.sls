include:
  - apt.transport.https
  - apt.dependencies

ffffm-repo:
  pkgrepo.managed:
    - comments:
      - "# FFFFM APT repo"
    - human_name: FFFFM repository
    - name: deb https://dl.ffm.freifunk.net/debian-packages/ sid main
    - dist: sid
    - file: /etc/apt/sources.list.d/ffffm.list
    - clean_file: True
    - key_url: salt://apt/files/keys/info-ffffm.gpg
    - require:
      - pkg: python-apt
      - pkg: apt-transport-https
