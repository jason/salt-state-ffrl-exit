include:
  - apt.transport.https
  - apt.dependencies

neoraider-repo:
  pkgrepo.managed:
    - comments:
      - "# neoraider APT repo"
    - human_name: neoraider repository
    - name: deb https://repo.universe-factory.net/debian/ sid main
    - dist: sid
    - file: /etc/apt/sources.list.d/neoraider.list
    - clean_file: True
    - key_url: salt://apt/files/keys/neoraider.gpg
    - require:
      - pkg: python-apt
      - pkg: apt-transport-https
