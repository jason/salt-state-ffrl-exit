include:
  - apt.transport.https
  - apt.dependencies

nodejs:
  pkgrepo.managed:
    - humanname: nodejs
    - name: deb https://deb.nodesource.com/node_8.x stretch main
    - key_url: salt://apt/files/keys/nodejs.gpg
    - file: /etc/apt/sources.list.d/nodejs.list
    - clean_file: True
    - require_in:
      - pkg: nodejs
    - require:
      - pkg: apt-transport-https
      - pkg: python-apt
  pkg.latest:
    - pkgs:
      - nodejs
