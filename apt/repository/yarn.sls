include:
  - apt.transport.https
  - apt.dependencies

yarn:
  pkgrepo.managed:
    - humanname: yarn
    - name: deb https://dl.yarnpkg.com/debian/ stable main
    - key_url: salt://apt/files/keys/yarn.gpg
    - file: /etc/apt/sources.list.d/yarn.list
    - clean_file: True
    - require_in:
      - pkg: yarn
    - require:
      - pkg: apt-transport-https
      - pkg: python-apt
  pkg.latest:
    - pkgs:
      - yarn
