include:
  - apt.dependencies

saltstack-repo:
  pkgrepo.managed:
    - humanname: repo.saltstack.com
{%- if grains['osfinger'] == 'Debian-8' %}
    - name: deb http://repo.saltstack.com/apt/debian/8/amd64/latest jessie main
{%- else %}
    - name: deb http://repo.saltstack.com/apt/debian/9/amd64/latest stretch main
{%- endif %}
    - file: /etc/apt/sources.list.d/saltstack.list
    - clean_file: True
    - key_url: https://repo.saltstack.com/apt/debian/9/amd64/latest/SALTSTACK-GPG-KEY.pub
    - require:
      - pkg: python-apt
