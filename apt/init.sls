#
## APT
#

include:
  - apt.transport.https
  - apt.unattended-upgrades
  - apt.dependencies

{% if grains['osfinger'] == "Debian-8" %}
/etc/apt/sources.list:
  file.managed:
    - contents: |
        deb http://deb.debian.org/debian jessie main contrib non-free
        deb-src http://deb.debian.org/debian jessie main

        deb http://security.debian.org/ jessie/updates main contrib non-free
        deb-src http://security.debian.org/ jessie/updates main

        deb http://deb.debian.org/debian jessie-updates main contrib non-free
        deb-src http://deb.debian.org/debian jessie-updates main

jessie-backports:
  pkgrepo.managed:
    - name: deb http://deb.debian.org/debian jessie-backports main contrib non-free
    - file: /etc/apt/sources.list.d/backports.list
    - clean_file: True
{% else %}
jessie-backports:
  pkgrepo.absent
{% endif %}

{% if grains['osfinger'] == "Debian-9" %}
/etc/apt/sources.list:
  file.managed:
    - contents: |
        deb http://deb.debian.org/debian stretch main contrib non-free
        deb-src http://deb.debian.org/debian stretch main

        deb http://security.debian.org/debian-security stretch/updates main contrib non-free
        deb-src http://security.debian.org/debian-security stretch/updates main

        deb http://deb.debian.org/debian stretch-updates main contrib non-free
        deb-src http://deb.debian.org/debian stretch-updates main

stretch-backports:
  pkgrepo.managed:
    - name: deb http://deb.debian.org/debian stretch-backports main contrib non-free
    - file: /etc/apt/sources.list.d/backports.list
    - clean_file: True
{% else %}
stretch-backports:
  pkgrepo.absent
{% endif %}
