include:
  - apt.transport.https

grafana:
  pkgrepo.managed:
    - humanname: grafana
{%- if grains.osfinger == 'Debian-8' %}
    - name: deb https://packagecloud.io/grafana/stable/debian/ jessie main
{%- else %}
    - name: deb https://packagecloud.io/grafana/stable/debian/ stretch main
{%- endif %}
    - key_url: https://packagecloud.io/gpg.key
    - file: /etc/apt/sources.list.d/grafana.list
  pkg.installed: []

grafana-server:
  service.running:
    - enable: True
    - watch:
      - file: /etc/default/grafana-server
      - file: /etc/grafana/grafana.ini
    - require:
      - pkg: grafana

/etc/default/grafana-server:
  file.managed:
    - source: salt://grafana/files/grafana-server-default
    - user: root
    - group: root
    - mode: 644
    - require:
      - pkg: grafana

/etc/grafana/grafana.ini:
  file.managed:
    - source: salt://grafana/files/grafana.ini
    - user: root
    - group: root
    - mode: 644
    - require:
      - pkg: grafana

prometheus_ffm_grafana:
  grafana4_datasource.present:
    - name: prometheus.ffm.freifunk.net
    - type: prometheus
    - url: https://prometheus.ffm.freifunk.net
    - access: proxy
    - is_default: true

graphite_ffm_grafana:
  grafana4_datasource.present:
    - name: graphite.ffm.freifunk.net
    - type: graphite
    - url: https://graphite.ffm.freifunk.net
    - access: proxy
    - is_default: false
