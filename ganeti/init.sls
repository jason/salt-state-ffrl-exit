{%- if grains['osfinger'] == 'Debian-9' %}
include:
  - apt.repository.sid

# upgrade ganeti from sid
ganeti-apt-pin:
  file.accumulated:
    - name: apt.sid.pinning_exceptions
    - filename: /etc/apt/preferences.d/sid-pinning
    - text: ganeti*
    - require_in:
      - file: /etc/apt/preferences.d/sid-pinning
{%- endif %}

ganeti:
  pkg.installed:
    - pkgs:
      - ganeti
      - ganeti-instance-debootstrap
{%- if grains['osfinger'] == 'Debian-8' %}
    - fromrepo: jessie-backports
{%- elif grains['osfinger'] == 'Debian-9' %}
    - fromrepo: sid
    - require:
      - pkgrepo: sid
{%- endif %}

{% set hypervisor = salt['pillar.get']('ganeti:%s:hypervisor'|format(salt['pillar.get']('ganeti:clustername'))) %}

ganeti_dependencies:
  pkg.installed:
    - pkgs:
      - drbd-utils
{%- if hypervisor == 'kvm' %}
      - qemu-kvm
{%- elif hypervisor == 'xen-pvm' %}
      - xen-system-amd64
{%- endif %}


# drbd replication
/etc/modprobe.d/drbd.conf:
  file.managed:
    - contents: options drbd minor_count=128 usermode_helper=/bin/true

drbd:
  kmod.present:
    - persist: True

# lvm: don't scan drbd devices for lvm volumes
/etc/lvm/lvm.conf:
  file.managed:
    - source: salt://ganeti/files/lvm.conf.{{ grains['oscodename'] }}

# firewalling on the cluster management network
/etc/ferm/conf.d/40-ganeti.conf:
  file.managed:
    - source: salt://ganeti/files/ferm.conf.j2
    - template: jinja
    - user: root
    - group: root
    - mode: 0644

# instance-debootstrap configuration (hooks & variants)
/etc/ganeti/instance-debootstrap/hooks:
  file.recurse:
    - source: salt://ganeti/files/hooks
    - clean: True
    - file_mode: 0755
    - dir_mode: 0755
    - require:
      - pkg: ganeti

/etc/ganeti/instance-debootstrap/variants.list:
  file.managed:
    - source: salt://ganeti/files/variants.list.j2
    - template: jinja
    - require:
      - pkg: ganeti

/etc/ganeti/instance-debootstrap/variants:
  file.recurse:
    - source: salt://ganeti/files/variants
    - clean: True
    - file_mode: 0644
    - dir_mode: 0755
    - require:
      - pkg: ganeti

/etc/ganeti/kvm-vif-bridge:
  file.managed:
    - source: salt://ganeti/files/kvm-vif-bridge
    - user: root
    - group: root
    - mode: 0755
