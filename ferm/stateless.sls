# prevent conntrack from being loaded
/etc/modprobe.d/blacklist_conntrack.conf:
  file.managed:
    - contents: blacklist nf_conntrack
    - user: root
    - group: root
    - mode: 644

# don't pass bridge traffic through iptables
br_netfilter:
  kmod.present:
    - persist: True

net.bridge.bridge-nf-call-iptables:
  sysctl.present:
    - value: 0
    - config: /etc/sysctl.d/br-nf.conf
    - require:
      - kmod: br_netfilter

net.bridge.bridge-nf-call-ip6tables:
  sysctl.present:
    - value: 0
    - config: /etc/sysctl.d/br-nf.conf
    - require:
      - kmod: br_netfilter

/etc/ferm/ferm.conf:
  file.managed:
    - source: salt://ferm/files/ferm-stateless.conf.j2
    - user: root
    - group: root
    - mode: 644
    - makedirs: True
    - template: jinja
