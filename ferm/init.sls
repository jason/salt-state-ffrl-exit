/etc/ferm/conf.d:
  file.directory:
    - user: root
    - group: root

# recursively delete legacy ferm.d folder
/etc/ferm.d:
  file.absent:
    # execute this last to ensure we first reinstall our ferm setup
    - order: last

# this fixes issues with file globbing, see https://github.com/saltstack/salt/issues/24436
/etc/ferm/conf.d/.keep:
  file.managed:
    - replace: False

# deprecated: dependency issues on boot made us move to the systemd unit
/etc/default/ferm:
  file.managed:
    - source: salt://ferm/files/ferm

/etc/systemd/system/ferm.service:
  file.managed:
    - source: salt://ferm/files/ferm.service
    - user: root
    - group: root
    - mode: 644

ferm:
  pkg.installed:
    - pkgs:
      - ferm
  service.running:
    - enable: True
    - require:
      - file: /etc/systemd/system/ferm.service
    - watch:
      - file: /etc/ferm/ferm.conf
      - file: /etc/ferm/conf.d/*

include:
{%- if 'vmhost' in pillar.roles %}
  - ferm.stateless
{%- elif 'edge' in pillar.roles %}
  - ferm.stateless
{%- elif 'noconntrack' in pillar.roles %}
  - ferm.stateless
{%- else %}
  - ferm.stateful
{% endif %}
