# for each domain
{%- for domain in pillar.get('domains', {}) %}

# assign fastd configuration
{%- set fastd = salt['pillar.get']('domains:%s:fastd'|format(domain)) %}

# open up ports in the firewall
/etc/ferm/conf.d/40-fastd-{{ domain }}.conf:
  file.managed:
    - source: salt://fastd/files/ferm.conf.j2
    - user: root
    - group: root
    - mode: 644
    - template: jinja
    - context:
        domain: {{ domain }}
    - require:
      - file: /etc/ferm/conf.d

# and for each instance (varying mtu/port)
{%- for instance in fastd['instances'] %}
{%- set fastd_config_root = '/etc/fastd/{}{}'.format(domain, instance['mtu']) %}

# create working directory
{{ fastd_config_root }}:
  file.directory:
    - mode: 755
    - makedirs: True

# deploy fastd.conf
{{ fastd_config_root }}/fastd.conf:
  file.managed:
    - source:
      - salt://fastd/files/fastd-{{ domain }}.conf.j2
      - salt://fastd/files/fastd-default.conf.j2
    - user: root
    - group: root
    - mode: 600
    - template: jinja
    - context:
        domain: {{ domain }}
        port: {{ instance['port'] }}
        mtu: {{ instance['mtu'] }}
        secret: {{ fastd['secret'] }}

# deploy verify
{{ fastd_config_root }}/verify:
  file.managed:
    - source:
      - salt://fastd/files/verify-{{ domain }}.j2
      - salt://fastd/files/verify.j2
    - user: root
    - group: root
    - mode: 755
    - template: jinja
    - context:
        domain: {{ domain }}
        port: {{ instance['port'] }}
        mtu: {{ instance['mtu'] }}

# enable instances and watch for config changes
fastd@{{ domain }}{{ instance['mtu'] }}:
  service.running:
    - enable: True
    - require:
      - pkg: fastd
    - watch:
      - file: {{ fastd_config_root }}/fastd.conf
{%- endfor %}
{%- endfor %}
