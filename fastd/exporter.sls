{%- set gopath = pillar.get('golang:gopath', '/usr/local/go') %}
{%- set gopkg = 'git.darmstadt.ccc.de/ffda/fastd-exporter' %}

include:
  - golang

fastd-exporter:
  git.latest:
    - name: https://{{ gopkg }}
    - target: {{ gopath }}/src/{{ gopkg }}
  cmd.run:
    - cwd: {{ gopath }}/src/{{ gopkg }}
    - name: go get -v -u {{ gopkg }}
    - env:
        GOPATH: {{ pillar.get('golang:gopath', '/usr/local/go') }}
    - require:
      - pkg: golang
      - git: fastd-exporter
    - onchanges:
      - git: fastd-exporter
  service.running:
    - enable: True
    - require:
      - file: /etc/systemd/system/fastd-exporter.service
    - watch:
      - file: /etc/systemd/system/fastd-exporter.service
      - cmd: fastd-exporter

/etc/systemd/system/fastd-exporter.service:
  file.managed:
    - source: salt://fastd/files/fastd-exporter.service.j2
    - user: root
    - group: root
    - mode: 644
    - template: jinja

/etc/ferm/conf.d/40-fastd-exporter.conf:
  file.managed:
    - source: salt://fastd/files/ferm-fastd-exporter.conf.j2
    - user: root
    - group: root
    - mode: 644
    - template: jinja
    - require:
      - file: /etc/ferm/conf.d

prometheus_fastd_export:
  grains.present:
    - value: {{ grains.nodename }}:9281
