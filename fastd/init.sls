fastd:
{% if grains.osfinger == 'Debian-8' %}
  pkg.installed:
    - fromrepo: jessie-backports
    - require:
      - pkgrepo: jessie-backports
{% else %}
  pkg.installed
{% endif %}

tun:
  kmod.present:
    - persist: True

fastd_disable_generic_autostart:
  file.replace:
    - name: /etc/default/fastd
    - pattern: ^AUTOSTART=(.*)$
    - repl: AUTOSTART="none"
    - require:
      - pkg: fastd


include:
  - fastd.exporter
  - fastd.instances
