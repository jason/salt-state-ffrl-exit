babeld:
  pkg.installed:
    - name: babeld
  service.running:
    - enable: True
    - name: babeld
    - require:
      - pkg: babeld
    - watch:
      - file: /etc/babeld.conf
      - file: /etc/systemd/system/babeld.service

/etc/babeld.conf:
  file.managed:
    - source: salt://babeld/files/babeld.conf
    - template: jinja
    - user: root
    - group: root
    - mode: 644

/etc/systemd/system/babeld.service:
  file.managed:
    - source: salt://babeld/files/babeld.service
    - user: root
    - group: root
    - mode: 644

/etc/ferm/conf.d/50-babeld.conf:
   file.managed:
     - source: salt://babeld/files/ferm.conf
     - template: jinja
     - user: root
     - group: root
     - mode: 644
     - require:
       - pkg: ferm

/usr/local/bin/echotobabel:
   file.managed:
    - source: salt://babeld/files/echotobabel
    - user: root
    - group: root
    - mode: 755

include:
  - network.babel-tabels
  - apt.repository.ffffm
