icvpn-dependencies:
  pkg.installed:
    - pkgs:
      - python3-yaml
      - python3-requests
      - python3-prettytable
      - python3-jinja2

/var/lib/icvpn:
  file.directory

icvpn-meta:
  git.latest:
    - name: https://github.com/freifunk/icvpn-meta.git
    - target: /var/lib/icvpn/meta

icvpn-scripts:
  git.latest:
    - name: https://github.com/freifunk/icvpn-scripts.git
    - target: /var/lib/icvpn/scripts

/var/lib/icvpn/config:
  file.managed:
    - source: salt://icvpn/files/config
    - user: root
    - group: root
    - mode: 644
    - require:
      - file: /var/lib/icvpn

{% if grains['id'].startswith('icvpn') %}
/usr/local/sbin/icvpn-mkbgp:
  file.managed:
    - source: salt://icvpn/files/icvpn-mkbgp
    - user: root
    - group: root
    - mode: 700
    - require:
      - file: /var/lib/icvpn/config

/etc/cron.d/icvpn-bgp:
  file.managed:
    - source: salt://icvpn/files/icvpn-bgp.cron
    - user: root
    - group: root
    - mode: 644
    - require:
      - file: /usr/local/sbin/icvpn-mkbgp

/etc/ferm/conf.d/40-icvpn.conf:
  file.managed:
    - source: salt://icvpn/files/ferm.conf.j2
    - user: root
    - group: root
    - mode: 644
    - require:
       - pkg: ferm

include:
  - tinc
  - tinc.icvpn
  - bird
  - bird.icvpn
{% elif grains['id'].startswith('ns') %}
/usr/local/sbin/icvpn-mkdns:
  file.managed:
    - source: salt://icvpn/files/icvpn-mkdns
    - user: root
    - group: root
    - mode: 700
    - require:
      - file: /var/lib/icvpn/config

/etc/cron.d/icvpn-dns:
  file.managed:
    - source: salt://icvpn/files/icvpn-dns.cron
    - user: root
    - group: root
    - mode: 644
    - require:
      - file: /usr/local/sbin/icvpn-mkdns

icvpn-mkdns:
  cmd.run:
    - name: /usr/local/sbin/icvpn-mkdns
    - require:
      - file: /usr/local/sbin/icvpn-mkdns
      - git: icvpn-meta
      - git: icvpn-scripts
      - pkg: icvpn-dependencies
    - onchanges:
      - git: icvpn-meta
      - git: icvpn-scripts
{% endif %}
