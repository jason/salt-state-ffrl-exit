roles:
  - test
  - router
  - ffrl-exit
  - babel
  - gateway

pipe_main_export_filter: accept_all

ifaces:
  lo:
    prefixes:
#      - 10.126.255.1/32
#      - 2a06:8187:fb00::1/128
      - 2a03:2260:1004::1/128
  eth0:
    lldp: on
    prefixes:
      - 176.9.211.141/29
      - 2a01:4f8:151:2383::141/64
    gateway:
      - 176.9.211.137
      - fe80::1
    vrf: vrf_external

  gre_ffrl_dus_a:
    type: GRE_FFRL
    endpoint: 185.66.193.0
    tunnel-physdev: eth0
    prefixes:
      - 100.64.7.53/31
      - 2a03:2260:0:3bf::2/64

  # NAT IP
  nat:
    link-type: dummy
    prefixes:
      - 185.66.194.32/32
