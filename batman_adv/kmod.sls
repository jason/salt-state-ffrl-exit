---
{%- if pillar.batman_adv.get('commit', False) %}

include:
  - kernel

# build kmod from git when commit is given

batman-adv_pkgs:
  pkg.installed:
    - pkgs:
      - git
      - dkms
      - build-essential
      - pkg-config

batman-adv_git:
  git.latest:
    - name: git://git.open-mesh.org/batman-adv.git
    - target: /usr/src/batman-adv-{{ pillar.batman_adv.version }}
    - rev: {{ pillar.batman_adv.commit }}
    - require:
      - pkg: batman-adv_pkgs

/usr/src/batman-adv-{{ pillar.batman_adv.version }}/dkms.conf:
  file.managed:
    - source: salt://batman_adv/files/dkms.conf.j2
    - template: jinja
    - require:
      - git: batman-adv_git

batman-adv_dkms_add:
  cmd.run:
    - onchanges:
      - git: batman-adv_git
    - require:
      - pkg: packages_kernel
      - pkg: batman-adv_pkgs
      - file: /usr/src/batman-adv-{{ pillar.batman_adv.version }}/dkms.conf
    - name: dkms add -m batman-adv -v {{ pillar.batman_adv.version }}

batman-adv_dkms_build:
  cmd.run:
    - onchanges:
      - cmd: batman-adv_dkms_add
    - name: dkms build -m batman-adv -v {{ pillar.batman_adv.version }}

batman-adv_dkms_install:
  cmd.run:
    - onchanges:
      - cmd: batman-adv_dkms_build
    - name: dkms install -m batman-adv -v {{ pillar.batman_adv.version }}

{%- else %}

# else remove kmod and return to intree version

batman-adv_dkms_remove:
  cmd.run:
    - name: dkms remove batman_adv --all

{%- endif %}

# either way, make sure the module is loaded on boot

batman_adv:
  kmod.present:
    - persist: True
