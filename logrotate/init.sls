logrotate:
  pkg.installed:
    - pkgs:
      - logrotate

/etc/logrotate.conf:
  file.managed:
    - source: salt://logrotate/files/logrotate.conf
    - user: root
    - group: root
    - mode: 644
    - require:
      - pkg: logrotate
