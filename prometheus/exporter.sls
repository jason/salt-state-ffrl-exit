include:
  - apt
  - ferm

prometheus-node-exporter:
  pkg.installed:
{%- if grains['osfinger'] == 'Debian-8' %}
    - fromrepo: jessie-backports
    - require:
      - pkgrepo: jessie-backports
{%- elif grains['osfinger'] == "Debian-9" %}
    - fromrepo: stretch-backports
    - require:
      - pkgrepo: stretch-backports
{%- endif %}
    - pkgs:
      - prometheus-node-exporter
  service.running:
    - enable: True
    - watch:
      - file: /etc/default/prometheus-node-exporter

/etc/ferm/conf.d/40-prometheus.conf:
  file.managed:
    - source: salt://prometheus/files/ferm.conf
    - user: root
    - group: root
    - mode: 644
    - require:
      - pkg: ferm

/etc/default/prometheus-node-exporter:
  file.managed:
    - source: salt://prometheus/files/prometheus-node-exporter
    - user: root
    - group: root
    - mode: 644
    - require:
       - pkg: prometheus-node-exporter

/etc/tmpfiles.d/prometheus-node-exporter-textfiles.conf:
  file.managed:
   - contents: |
           #Type Path                                     Mode UID  GID  Age Argument
           d     /run/prometheus-node-exporter/textfiles/ 0755 root root -   -
   - user: root
   - group: root
   - mode: 644
   - require:
     - pkg: prometheus-node-exporter

reload tmpfiles:
  cmd.run:
    - name: systemd-tmpfiles --create
    - onchanges:
      - file: /etc/tmpfiles.d/prometheus-node-exporter-textfiles.conf
