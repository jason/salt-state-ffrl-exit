groups:
- name: alert.rules
  rules:
  - alert: InstanceDown
    expr: up{job="node"} == 0
    for: 3m
    labels:
      severity: page
    annotations:
      description: 'The instance is down for more than 3 minutes'
      summary: 'Instance {{ $labels.instance }} is down'

  - alert: ExporterDown
    expr: up{job!="node"} == 0
    for: 5m
    annotations:
      description: 'An exporter is down for more than 5 minutes'
      summary: 'Exporter {{ $labels.instance }} is down'

  - alert: InstanceHighCpu
    expr: 100 - (avg(rate(node_cpu{mode="idle"}[5m])) BY (instance) * 100) > 90
    for: 5m
    annotations:
      description: 'CPU usage above 90% for more than 5m'
      summary: 'Instance {{ $labels.instance }}: cpu usage at {{ $value }}'
      value: '{{ $value }}'

  - alert: InstanceHighCpuLong
    expr: 100 - (avg(rate(node_cpu{mode="idle"}[5m])) BY (instance) * 100) > 90
    for: 30m
    labels:
      severity: page
    annotations:
      description: 'CPU usage above 90% for more than 30m'
      summary: 'Instance {{ $labels.instance }}: persistent cpu usage at {{ $value }}'
      value: '{{ $value }}'

  - alert: InstanceLowMem
    expr: node_memory_MemAvailable / 1024 / 1024 < node_memory_MemTotal / 1024 / 1024 / 10
    for: 3m
    labels:
      severity: page
    annotations:
      description: 'Less than 10% of free memory'
      summary: 'Instance {{ $labels.instance }}: {{ $value }}MB of free memory'
      value: '{{ $value }}'

  - alert: InstanceLowDiskAbs
    expr: node_filesystem_avail{mountpoint="/"} / 1024 / 1024 < 1024
    for: 1m
    labels:
      severity: page
    annotations:
      description: 'Less than 1GB of free disk space left on the root filesystem'
      summary: 'Instance {{ $labels.instance }}: {{ $value }}MB free disk space on {{ $labels.device }}'
      value: '{{ $value }}'

  - alert: InstanceLowDiskPerc
    expr: 100 * (node_filesystem_free / node_filesystem_size) < 10
    for: 1m
    labels:
      severity: page
    annotations:
      description: 'Less than 10% of free disk space left on a device'
      summary: 'Instance {{ $labels.instance }}: {{ $value }}% free disk space on {{ $labels.device }}'
      value: '{{ $value }}'

  - alert: RaidResync
    expr: (node_md_blocks_synced / node_md_blocks) * 100 < 100
    labels:
      severity: warning
    annotations:
      description: 'An md-raid device is not in sync'
      summary: 'Instance {{ $labels.instance }}: {{ $labels.device }} is at {{ $value }}% sync'
      value: '{{ $value }}'

  - alert: BondMissingSlave
    expr: node_net_bonding_slaves_active < node_net_bonding_slaves
    labels:
      severity: page
    annotations:
      description: 'A bond device is missing one or more of its slave interfaces.'
      summary: 'Instance {{ $labels.instance }}: {{ $labels.master }} missing {{ $value }} slave interface(s)'
      value: '{{ $value }}'

  - alert: ServiceFailed
    expr: node_systemd_unit_state{state="failed"} > 0
    for: 1m
    labels:
      severity: page
    annotations:
      description: 'A systemd unit went into failed state'
      summary: 'Instance {{ $labels.instance }}: Service {{ $labels.name }} failed'
      value: '{{ $labels.name }}'

  - alert: ServiceFlapping
    expr: changes(node_systemd_unit_state{state="active"}[5m]) > 5 or
     (changes(node_systemd_unit_state{state="active"}[1h]) > 15 unless changes(node_systemd_unit_state{state="active"}[30m]) < 7)
    labels:
      severity: page
    annotations:
      description: 'A systemd service changed its state more than 5x/5min or 15x/1h'
      summary: 'Instance {{ $labels.instance }}: Service {{ $labels.name }} is flapping'
      value: '{{ $labels.name }}'

  - alert: OutdatedLibs
    expr: checkrestart_process_count > 0
    labels:
      severity: warning
    annotations:
      description: 'Number of processes which run on outdated libs'
      summary: 'Instance {{ $labels.instance }}: There are {{ $value }} processes with old libs.'
      value: '{{ $value }}'

  - alert: FastdPeerLimit
    expr: (sum(fastd_peer_up) BY (interface) / sum(fastd_up) BY (interface)) > 100 * 0.95
    labels:
      severity: warning
    annotations:
      description: 'A fastd interface reached its peer limit'
      summary: 'Peer connection count for {{ $labels.interface }} at {{ $value }} per gateway.'
      value: '{{ $value }}'

  - alert: BGP4SessionDown
    expr: avg_over_time(bird_bgp4_session_up[1d]) == 0
    labels:
      severity: warning
    annotations:
      description: 'A BGP IPv4 session is down for more than 1 day'
      summary: 'Instance {{ $labels.instance }}: BGP4 {{ $labels.name }} down'
      value: '{{ $labels.name }}'

  - alert: BGP6SessionDown
    expr: avg_over_time(bird_bgp6_session_up[1d]) == 0
    labels:
      severity: warning
    annotations:
      description: 'A BGP IPv6 session is down for more than 1 day'
      summary: 'Instance {{ $labels.instance }}: BGP6 {{ $labels.name }} down'
      value: '{{ $labels.name }}'

  - alert: BGP4SessionFlapping
    expr: changes(bird_bgp4_session_up[5m]) > 5 or
     (changes(bird_bgp4_session_up[1h]) > 15 unless changes(bird_bgp4_session_up[30m]) < 7)
    labels:
      severity: page
    annotations:
      description: 'A BGP IPv4 session changed its state more than 5x/5min or 15x/1h'
      summary: 'Instance {{ $labels.instance }}: BGP4 {{ $labels.name }} is flapping'
      value: '{{ $labels.name }}'

  - alert: BGP6SessionFlapping
    expr: changes(bird_bgp6_session_up[5m]) > 5 or
     (changes(bird_bgp6_session_up[1h]) > 15 unless changes(bird_bgp6_session_up[30m]) < 7)
    labels:
      severity: page
    annotations:
      description: 'A BGP IPv6 session changed its state more than 5x/5min or 15x/1h'
      summary: 'Instance {{ $labels.instance }}: BGP6 {{ $labels.name }} is flapping'
      value: '{{ $labels.name }}'

  - alert: FastdBackgroundNoise
    expr: (min(irate(fastd_peer_tx_bytes{}[1m])>1) by (interface)) / 1024 > 30
    for: 5m
    labels:
      severity: page
    annotations:
      description: 'A fastd interface background noise is over 30 kBps for 5 min'
      summary: 'Fastd background noise for {{ $labels.interface }} is at {{ $value }}.'
      value: '{{ $value }}'

  - alert: BGP4TransitSessionDown
    expr: count((avg_over_time(bird_bgp4_session_up{name=~"er.*of_aixit_net"}[1m]) == 0)) >1
    labels:
      severity: page
    annotations:
      description: '>=2 BGP IPv4 Transit session are down for more than 1 minute'
      summary: 'Instance {{ $labels.instance }}: BGP4 {{ $labels.name }} down'
      value: '{{ $labels.name }}'

  - alert: BGP6TransitSessionDown
    expr: count((avg_over_time(bird_bgp6_session_up{name=~"er.*of_aixit_net|Hurricane_Electric_KLEYREX"}[1m]) == 0)) >1
    labels:
      severity: page
    annotations:
      description: '>=2 BGP IPv6 Transit session are down for more than 1 minute'
      summary: 'Instance {{ $labels.instance }}: BGP6 {{ $labels.name }} down'
      value: '{{ $labels.name }}'

  - alert: BGP4AixitSessionDown
    expr: count((avg_over_time(bird_bgp4_session_up{name=~"er.*of_aixit_net"}[1m]) == 1)) < 1
    labels:
      severity: page
    annotations:
      description: 'All BGP IPv4 Aixit session are down for more than 1 minute'
      summary: 'BGP4 Aixit down'

  - alert: BGP6AixitSessionDown
    expr: count((avg_over_time(bird_bgp6_session_up{name=~"er.*of_aixit_net"}[1m]) == 1)) < 1
    labels:
      severity: page
    annotations:
      description: 'All BGP IPv6 Aixit session are down for more than 1 minute'
      summary: 'BGP6 Aixit down'
