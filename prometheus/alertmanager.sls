{%- from 'prometheus/map.jinja' import prometheus with context -%}

alertmanager tarball:
  archive.extracted:
    - source: https://github.com/prometheus/alertmanager/releases/download/v{{ prometheus['alertmanager_release'] }}/alertmanager-{{ prometheus['alertmanager_release'] }}.linux-amd64.tar.gz
    - source_hash: sha256={{ prometheus['alertmanager_release_hash'] }}
    - if_missing: /opt/alertmanager-{{ prometheus['alertmanager_release'] }}.linux-amd64
    - name: /opt
    - user: root
    - group: root
    - watch_in:
      - service: prometheus-alertmanager.service

/var/lib/prometheus/alertmanager:
  file.directory:
    - user: prometheus
    - group: prometheus

/etc/systemd/system/prometheus-alertmanager.service:
  file.managed:
    - source: salt://prometheus/files/prometheus-alertmanager.service.j2
    - template: jinja
    - watch_in:
      - service: prometheus-alertmanager.service

/etc/prometheus/alertmanager.yml:
  file.managed:
    - source: salt://prometheus/files/alertmanager.yml.j2
    - user: root
    - group: root
    - mode: 644
    - template: jinja
    - watch_in:
      - service: prometheus-alertmanager.service

/etc/default/prometheus-alertmanager:
  file.managed:
   - source: salt://prometheus/files/prometheus-alertmanager
   - user: root
   - group: root
   - mode: 644
   - watch_in:
     - service: prometheus-alertmanager.service

prometheus-alertmanager.service:
  service.running:
    - enable: True
    - require:
      - archive: alertmanager tarball
      - file: /var/lib/prometheus/alertmanager
