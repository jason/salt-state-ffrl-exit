include:
  # install node exporter first, it creates a prometheus user, so we don't have to
  - prometheus.exporter
  - prometheus.checkrestart
{%- if 'prometheus' in pillar.get('roles', []) %}
  - prometheus.prometheus
  - prometheus.alertmanager
  - prometheus.irc
{%- endif %}
