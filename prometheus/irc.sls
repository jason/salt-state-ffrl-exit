{%- set gopath = pillar.get('golang:gopath', '/usr/local/go') %}
{%- set project = 'go-prom-irc' %}
{%- set uri = 'github.com/freifunk-darmstadt/go-prom-irc' %}

include:
  - golang

{{ project }}:
  git.latest:
    - name: https://{{ uri }}
    - target: /usr/src/{{ project }}
  cmd.run:
    - name: go get -v -u {{ uri }}
    - env:
        GOPATH: {{ gopath }}
    - require:
      - pkg: golang
      - git: {{ project }}
    - onchanges:
      - git: {{ project }}
  service.running:
    - enable: True
    - require:
      - file: /etc/systemd/system/{{ project }}.service
    - watch:
      - file: /etc/systemd/system/{{ project }}.service
      - cmd: {{ project }}

/etc/systemd/system/{{ project }}.service:
  file.managed:
    - source: salt://prometheus/files/{{ project }}.service.j2
    - user: root
    - group: root
    - mode: 644
    - template: jinja
    - context:
        gopath: {{ gopath }}
