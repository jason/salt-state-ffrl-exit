icvpn:
  git.latest:
    - name: https://github.com/freifunk/icvpn.git
    - target: /etc/tinc/icvpn

/etc/tinc/icvpn/rsa_key.priv:
  file.managed:
    - user: root
    - group: root
    - mode: 0600
    - contents: |
        {{ pillar['icvpn']['rsa_key'] | indent(8) }}

/etc/tinc/icvpn/ed25519_key.priv:
  file.managed:
    - user: root
    - group: root
    - mode: 0600
    - contents: |
        {{ pillar['icvpn']['ed25519_key'] | indent(8) }}

/etc/tinc/icvpn/tinc.conf:
  file.managed:
    - source: salt://tinc/files/tinc-icvpn.conf.j2
    - user: root
    - group: root
    - mode: 644
    - template: jinja

/etc/tinc/icvpn/tinc-up:
  file.managed:
    - source: salt://tinc/files/icvpn-up.j2
    - user: root
    - group: root
    - mode: 755
    - template: jinja

/etc/tinc/icvpn/.git/hooks/post-merge:
  file.symlink:
    - target: /etc/tinc/icvpn/scripts/post-merge
  cmd.script:
    - shell: /bin/bash
    - cwd: /etc/tinc/icvpn
    - require:
      - file: /etc/tinc/icvpn/tinc.conf
    - onchanges:
      - git: icvpn

/etc/ferm/conf.d/40-tinc-icvpn.conf:
  file.managed:
    - source: salt://tinc/files/ferm.conf.j2
    - user: root
    - group: root
    - mode: 644
    - template: jinja
    - context:
      port: {{ pillar['icvpn']['port'] }}

tinc.service:
  service.running:
    - enable: True

tinc@icvpn:
  service.running:
    - enable: True
    - require:
      - pkg: tinc
    - watch:
      - file: /etc/tinc/icvpn/tinc.conf
