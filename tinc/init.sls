{% if grains['oscodename'] == 'stretch' %}
debian-experimental:
  pkgrepo.managed:
    - name: deb http://deb.debian.org/debian experimental main
    - file: /etc/apt/sources.list.d/experimental.list
    - clean_file: True
    - require:
      - file: /etc/apt/preferences.d/tinc

/etc/apt/preferences.d/tinc:
  file.managed:
    - user: root
    - group: root
    - mode: 0644
    - contents: |
        Package: tinc
        Pin: release a=experimental
        Pin-Priority: 800

tinc:
  pkg.installed:
    - fromrepo: experimental

{% else %}
tinc:
  pkg.installed

{% endif %}
