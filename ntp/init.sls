chrony:
    pkg.installed:
      - name: chrony
    service.running:
      - enable: True
      - name: chrony

/etc/chrony/chrony.conf:
  file.managed:
    - source: salt://ntp/files/chrony_config.j2
    - template: jinja
    - user: root
    - group: root
    - mode: 644
    - watch_in:
      - service: chrony

{%- if 'ntp' in pillar.get('roles', []) %}
/etc/ferm/conf.d/40-chrony.conf:
  file.managed:
    - source: salt://ntp/files/ferm.conf
    - user: root
    - group: root
    - mode: 644
    - template: jinja
    - require:
      - file: /etc/ferm/conf.d
{%- endif %}
