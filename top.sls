base:
  '*':
    - ssh
    - apt
    - ferm
    - salt.minion
    - mosh
    - common.debian
    - common.packages
    - common.datetime
    - common.dotfiles
    - common.locales
    - common.systemd
    - common.userfreifunk
    - common.ffadmin
    - common.sudo
    - ntp
    - prometheus
    - logrotate
  'test.ffm.freifunk.net':
    - lldp
  'virtual:physical':
    - match: grain
    - lldp
  'roles:vmhost':
    - match: pillar
    - ganeti
    - bird
    - bird.bgp
    - bird.ospf
    - kernel.sysctl
    - network
  'roles:kresd':
    - match: pillar
    - dnsresolver.kresd
    - bird
    - bird.bgp
    - bird.ospf
    - kernel.sysctl
    - network
  'roles:unbound':
    - match: pillar
    - dnsresolver.unbound
    - bird
    - bird.bgp
    - bird.ospf
    - kernel.sysctl
    - network
  'roles:gitlab-ci':
    - match: pillar
    - gitlab-ci
    - kernel
    - kernel.sysctl
  'roles:icvpn':
    - match: pillar
    - bird
    - tinc
    - tinc.icvpn
    - icvpn
    - kernel.sysctl
    - network
  'roles:dn42':
    - match: pillar
    - bird
    - wireguard
    - wireguard.dn42
    - kernel.sysctl
    - network
  'salt.ffm.freifunk.net':
    - salt.master
  'edge-*-*.ffm.freifunk.net':
    - bird
    - bird.bgp
    - bird.ospf
    - kernel.sysctl
    - network
  'access-*.batman.ffm.freifunk.net':
    - bird
    - bird.bgp
    - bird.ospf
    - batman
    - network
    - kernel
    - kernel.sysctl
  'prometheus.ffm.freifunk.net':
    - letsencrypt
    - nginx
  'a.ns.as64475.net':
    - knot-dns
  'b.ns.as64475.space':
    - knot-dns
  'c.ns.ffffm.net':
    - knot-dns
  'd.ns.freifunk-frankfurt.de':
    - knot-dns
  'core*.*.*.*.as64475.net':
    - bird
    - bird.bgp
    - bird.ospf
    - kea-dhcp
    - kernel.sysctl
    - network
  'rr*.as64475.net':
    - bird
    - bird.bgp
    - bird.ospf
    - kernel.sysctl
    - network
  'www2.aixit.off.de.ffffm.net':
    - nginx
    - letsencrypt
    - meshviewer
    - api
    - grafana
    - kernel
    - kernel.sysctl
  'graphite1.aixit.off.de.ffffm.net':
    - graphite
    - nginx
    - letsencrypt
    - kernel
    - kernel.sysctl
  'prometheus1.aixit.off.de.ffffm.net':
    - letsencrypt
    - nginx
  'access2.legacy.aixit.off.de.ffffm.net':
    - bird
    - bird.bgp
    - bird.ospf
    - batman
    - kernel
    - kernel.sysctl
    - mesh-announce
    - network
    - network.batman-adv
    - network.domains-batman
  'yanic.legacy.aixit.off.de.ffffm.net':
    - yanic
    - batman
    - nginx
    - letsencrypt
    - kernel
    - kernel.sysctl
    - mesh-announce
    - network
    - network.batman-adv
    - network.domains-batman
  'gw*.babel.*.*.*.ffffm.net':
    - bird
    - bird.bgp
    - bird.ospf
    - kernel
    - kernel.sysctl
    - network
    - network.domains-babel
    - mmfd
    - l3roamd
    - babeld
    - fastd
  'gw*.legacy.aixit.off.de.ffffm.net':
    - bird
    - bird.bgp
    - bird.ospf
    - fastd
    - kea-dhcp
    - batman
    - mesh-announce
    - kernel
    - kernel.sysctl
    - network
    - network.batman-adv
    - network.domains-batman
  'www3.aixit.off.de.ffffm.net':
    - nginx
    - letsencrypt
    - kernel
    - kernel.sysctl
  'jool*.aixit.off.de.ffffm.net':
    - bird
    - bird.ospf
    - kernel
    - kernel.sysctl
  'gw*.batman15.aixit.off.de.ffffm.net':
    - bird
    - bird.bgp
    - bird.ospf
    - fastd
    - kea-dhcp
    - batman_adv
    - mesh-announce
    - kernel
    - kernel.sysctl
    - network
  'yanic.batman15.aixit.off.de.ffffm.net':
    - yanic
    - batman_adv
    - nginx
    - letsencrypt
    - mesh-announce
    - kernel
    - kernel.sysctl
    - network
  'netbox.aixit.off.de.as64475.net':
    - netbox
