[Unit]
Description=l3roamd
Wants=basic.target
After=basic.target network.target fastd.service babeld.service

[Service]
Type=simple
ExecStart=/usr/local/bin/l3roamd -p 2a06:8187:fbab:2::/64  {% for name, interface in pillar.ifaces.items() if 'babel' in interface %} -m {{ name }} {% endfor %} {% for name in salt['fastd.all_instances_name']() %} -m {{ name }} {% endfor %} -t 11 -a {{ pillar.l3roamd.nodeip }} -4 0:0:0:0:0:ffff::/96
KillMode=process
ExecStartPost=/usr/local/bin/execafter_ifup l3roam0 "/sbin/ip -6 route add 2a06:8187:fbab:2::/64 dev l3roam0 table 10"
ExecStartPost=/usr/local/bin/execafter_ifup l3roam0 "/sbin/ip -6 route add 2a06:8187:fbab:1::/64 dev l3roam0 table 10"
Restart=always
RestartSec=3

[Install]
WantedBy=multi-user.target
