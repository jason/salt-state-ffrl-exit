l3roamd:
  pkg.installed:
    - name: l3roamd

/etc/systemd/system/l3roamd.service:
  file.managed:
    - template: jinja
    - source: salt://l3roamd/files/l3roamd.service.tpl
    - user: root
    - group: root
    - mode: 644
    - template: jinja
    - require:
      - file: /usr/local/bin/execafter_ifup

/usr/local/bin/execafter_ifup:
  file.managed:
    - source: salt://l3roamd/files/execafter_ifup
    - user: root
    - group: root
    - mode: 755

/etc/ferm/conf.d/41-l3roamd.conf:
  file.managed:
    - template: jinja
    - source: salt://l3roamd/files/ferm.conf
    - user: root
    - group: root
    - mode: 644
    - require:
      - pkg: ferm

/etc/network/interfaces.d/l3roamd:
  file.managed:
    - source: salt://l3roamd/files/l3roamd-network
    - user: root
    - group: root
    - template: jinja
    - mode: 644

include:
  - network.babel-tabels
  - apt.repository.ffffm
