include:
  - apt.repository.saltstack

salt-minion:
  pkg.installed:
    - fromrepo: saltstack-repo
  service.running:
    - enable: True
    - watch:
      - file: /etc/salt/minion.d

/etc/salt/minion.d:
  file.recurse:
    - source: salt://salt/files/minion.d
    # cannot clean because the salt-minion recreates /etc/salt/minion.d/_schedule.conf so it would always be unclean
    # - clean: True

# deploy killmode workaround to fix broken minion upgrades
# https://github.com/saltstack/salt/issues/7997#issuecomment-160913751
/etc/systemd/system/salt-minion.service.d/killmode.conf:
  file.managed:
    - source: salt://salt/files/killmode.conf
    - makedirs: True
