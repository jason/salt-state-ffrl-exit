include:
  - apt.repository.saltstack

salt-master:
  pkg.installed:
    - fromrepo: saltstack-repo

/etc/ferm/conf.d/40-salt-zmq.conf:
  file.managed:
    - source: salt://salt/files/ferm.conf
    - user: root
    - group: root
    - mode: 644
