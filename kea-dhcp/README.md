Example Pillar:

```yaml
kea-dhcp:

  IPv4:
    interfaces:
      - eth1
    subnets:
      - subnet: 192.168.200.0/24
        pools:
          - 192.168.200.10 - 192.168.200.50
        options:
          - name: routers
            data: 192.168.200.5
    options:
      - name: domain-name-servers
        data: 8.8.8.8, 8.8.4.4

  IPv6:
    interfaces:
      - eth1
    subnets:
      - subnet: 2001:abc::/64
        pools:
          - 2001:abc::/64
      - subnet: 2001:db8::/64
        interface: eth2
        pools:
          - 2001:db8::/64
    options:
      - name: dns-servers
        data: 2001:4860:4860::8888, 2001:4860:4860::8844

domains:
  legacy:
    domain: clients.legacy.ffffm.net

    # see: https://lists.isc.org/pipermail/kea-users/2017-February/000871.html
    #search_binary: '066c656761637905666666666d036e6574000366666d086672656966756e6b036e657400'
    search:
      - legacy.ffffm.net
      - ffm.freifunk.net

    IPv4:
      subnets:
        10.126.0.0/16:
          ranges:
            - start: 10.126.0.10
              end: 10.126.3.254
              routers:
                - 10.126.0.1
      name_servers:
        - 185.206.208.56
        - 185.206.208.57
    IPv6:
      subnets:
        'fddd:5d16:b5dd::/64':
          network: 'fddd:5d16:b5dd::'
          prefix: 64
        '2a06:8187:fbba:1234::/64':
          network: '2a06:8187:fbba:1234::'
          prefix: 64
      name_servers:
      - 2a06:8187:fb56::1:56
      - 2a06:8187:fb57::1:57
```
