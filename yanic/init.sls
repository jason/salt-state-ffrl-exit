{%- set gopath = '/var/lib/yanic/go' %}

include:
  - golang
  - ferm

yanic:
  user.present:
    - home: /var/lib/yanic
    - gid_from_name: True
    - shell: /usr/sbin/nologin
  git.latest:
    - name: https://github.com/FreifunkBremen/yanic
    - target: {{ gopath }}/src/github.com/FreifunkBremen/yanic
    - force_fetch: True
    - refspec_branch: master
    - user: yanic
    - require:
      - user: yanic
  cmd.run:
    - cwd: {{ gopath }}/src/github.com/FreifunkBremen/yanic
    - name: go get -v -u github.com/FreifunkBremen/yanic
    - user: yanic
    - env:
        GOPATH: {{ gopath }}
    - require:
      - pkg: golang
      - git: yanic
    - onchanges:
      - git: yanic

/var/log/yanic:
  file.directory:
    - user: yanic
    - group: adm
    - dir_mode: 2750
    - require:
      - user: yanic

/etc/yanic:
  file.directory:
    - user: root
    - group: yanic
    - dir_mode: 0755

/var/lib/yanic/state:
  file.directory:
    - user: yanic
    - group: yanic
    - dir_mode: 0755

/etc/systemd/system/yanic.service:
  file.managed:
    - source: salt://yanic/files/yanic.service
    - user: root
    - group: root
    - mode: 0644

/etc/ferm/conf.d/40-yanic.conf:
  file.managed:
    - source: salt://yanic/files/ferm.conf.j2
    - user: root
    - group: root
    - mode: 0644
    - template: jinja
    - require:
      - file: /etc/ferm/conf.d

/etc/yanic/config.toml:
  file.managed:
    - source: salt://yanic/files/config.toml.j2
    - user: root
    - group: root
    - mode: 0644
    - template: jinja

yanic.service:
  service.running:
    - enable: True
    - require:
      - file: /etc/systemd/system/yanic.service
      - file: /var/log/yanic
      - file: /etc/yanic/config.toml
    - watch:
      - file: /etc/systemd/system/yanic.service
      - file: /etc/yanic/config.toml
      - cmd: yanic
