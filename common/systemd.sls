systemd_pkgs:
  pkg.installed:
    - pkgs:
      - systemd
      - dbus
      - libpam-systemd
