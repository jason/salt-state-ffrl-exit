packages_base:
  pkg.installed:
    - pkgs:
      - apt-listbugs
      - ca-certificates
      - curl
      - debian-goodies
      - dnsutils
      - htop
      - iftop
      - iperf3
      - iputils-tracepath
      - jq
      - man-db
      - mlocate
      - mtr-tiny
      - ncdu
      - ncurses-term
      - netcat-openbsd
      - psmisc
      - python-augeas
      - rsync
      - strace
      - tcpdump
      - tig
      - tmux
      - tree
      - vim-nox
      - wget
      - whois
      - zsh
{%- if grains['osfinger'] == "Debian-9" %}
      - needrestart
{%- endif %}

packages_purge:
  pkg.purged:
    - pkgs:
      - inetd
      - puppet
      - puppet-common
      - rsh-server
      - silversearcher-ag
      - telnet-server
      - tftp-server
      - xinetd
      - ypserv


# these installs have dedicted states, so they can be referenced in require statements
git:
  pkg.installed
