ffadmin:
  user.present:
    - shell: /bin/bash
    - groups:
      - sudo
      - ssh-user
    - require:
      - ssh-user


/home/ffadmin/.ssh/authorized_keys:
  file.managed:
    - source: salt://common/files/authorized_keys.tpl
    - user: ffadmin
    - group: ffadmin
    - mode: 600
    - makedirs: True
    - template: jinja

/home/ffadmin/.screenrc:
  file.managed:
    - source: salt://common/files/screenrc.root

