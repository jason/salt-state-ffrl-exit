locales:
  pkg.installed

en_locale:
  locale.present:
    - name: en_US.UTF-8

de_locale:
  locale.present:
    - name: de_DE.UTF-8


locales_reconfigure:
  cmd.wait:
    - name: /usr/sbin/dpkg-reconfigure -f noninteractive locales
    - watch:
      - locale: en_locale
      - locale: de_locale

default_locale:
  cmd.run:
    - name: /usr/sbin/update-locale LANG=en_US.UTF-8
    - onchanges:
      - cmd: locales_reconfigure

