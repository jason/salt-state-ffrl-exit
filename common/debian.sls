rpcbind:
  pkg.purged

purge-exim4:
  pkg.purged:
    - pkgs:
      - exim4
      - bsd-mailx
      - exim4-base
      - exim4-config
      - exim4-daemon-light

python-systemd:
  pkg.installed

{% if grains['virtual'] == 'physical' %}
firmware-linux:
  pkg.installed:
    - pkgs:
      - firmware-linux-free
      - firmware-linux-nonfree
{% endif %}
