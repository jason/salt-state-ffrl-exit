create user freifunk:
  user.present:
    - name: freifunk
    - shell: /bin/bash

create /var/freifunk:
  file.directory:
    - name: /var/freifunk
    - user: freifunk
    - group: freifunk
    - makedirs: true
