#
# Screen
#

screen:
  pkg.installed

/root/.screenrc:
  file.managed:
    - source: salt://common/files/screenrc.root

