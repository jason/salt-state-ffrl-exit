include:
  - docker
  - apt.transport.https
  - apt.dependencies

gitlab-runner:
  pkgrepo.managed:
    - humanname: gitlab-runner
    - name: deb https://packages.gitlab.com/runner/gitlab-runner/debian/ stretch main
    - file: /etc/apt/sources.list.d/runner_gitlab-runner.list
    - key_url: salt://gitlab-ci/files/gpgkey
    - require:
      - pkg: apt-transport-https
  file.managed:
    - name: /etc/apt/preferences.d/pin-gitlab-runner.pref
    - contents: |
        Explanation: Prefer GitLab provided packages over the Debian native ones
        Package: gitlab-runner
        Pin: origin packages.gitlab.com
        Pin-Priority: 1001
  pkg.installed: []
