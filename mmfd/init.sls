include:
  - apt.repository.ffffm

mmfd:
  pkg.installed:
    - name: mmfd
  service.running:
    - enable: True
    - name: mmfd
    - require:
      - pkg: babeld
      - pkg: mmfd

/etc/systemd/system/mmfd.service:
  file.managed:
    - source: salt://mmfd/files/mmfd.service
    - user: root
    - group: root
    - mode: 644
    - require:
      - file: /usr/local/bin/execafter_ifup

/etc/ferm/conf.d/41-mmfd.conf:
  file.managed:
    - source: salt://mmfd/files/ferm.conf
    - user: root
    - group: root
    - template: jinja
    - mode: 644
    - require:
      - pkg: ferm

/etc/network/interfaces.d/mmfd:
  file.managed:
    - source: salt://mmfd/files/mmfd-network
    - user: root
    - group: root
    - template: jinja
    - mode: 644
