{%- set sidKernel = false -%}
{%- if 'batman15' in pillar.roles or 'babel' in pillar.roles -%}
  {%- set sidKernel = true -%}
{%- endif -%}
{%- if sidKernel %}
include:
  - apt.repository.sid
{% endif %}

packages_kernel:
  pkg.latest:
{%- if sidKernel %}
    - fromrepo: sid
{%- elif grains['osfinger'] == "Debian-8" %}
    - fromrepo: jessie-backports
{%- elif grains['osfinger'] == "Debian-9" %}
    - fromrepo: stretch-backports
{%- endif %}
    - pkgs:
      - linux-image-amd64
      - linux-headers-amd64
      - linux-perf
      - iproute2
      - iproute2-doc

{%- if sidKernel %}
kernel-apt-pin:
  file.accumulated:
    - name: apt.sid.pinning_exceptions
    - filename: /etc/apt/preferences.d/sid-pinning
    - text: |
        linux-image-amd64
        linux-headers-amd64
        linux-perf
        iproute2*
    - require_in:
      - file: /etc/apt/preferences.d/sid-pinning
{%- endif %}

purge-apparmor:
  pkg.purged:
    - pkgs:
      - apparmor
      - libapparmor-perl
      - apparmor-profiles
      - apparmor-profiles-extra
      - apparmor-utils
