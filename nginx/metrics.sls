https://github.com/knyar/nginx-lua-prometheus.git:
  git.latest:
    - target: /usr/local/share/nginx-lua-prometheus
    - user: root

/etc/nginx/snippets/metrics.conf:
  file.managed:
    - source: salt://nginx/files/snippets/metrics.conf
    - user: root
    - group: root
    - mode: 644
    - require:
      - pkg: nginx
      - git: https://github.com/knyar/nginx-lua-prometheus.git

/etc/nginx/conf.d/metrics.conf:
  file.managed:
    - source: salt://nginx/files/conf.d/metrics.conf
    - user: root
    - group: root
    - mode: 644
    - require:
      - pkg: nginx
      - git: https://github.com/knyar/nginx-lua-prometheus.git

prometheus_nginx_export:
  grains.present:
    - require:
      - file: /etc/nginx/conf.d/metrics.conf
      - file: /etc/nginx/snippets/metrics.conf
    - value: {{ grains['fqdn'] }}:443

