---
fcgiwrap:
  pkg.latest:
    - pkgs:
      - fcgiwrap
  service.running:
    - enable: True
