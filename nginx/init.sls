nginx:
  pkg.installed:
{% if grains['osfinger'] == "Debian-8" %}
    - fromrepo: jessie-backports
{% endif %}
    - pkgs:
      - nginx-extras
  service.running:
    - enable: True
    - reload: True
    - watch:
      - file: /etc/nginx/sites-available/*
      - file: /etc/nginx/sites-enabled/*
      - file: /etc/nginx/snippets/*
      - file: /etc/nginx/conf.d/*


/etc/nginx/snippets:
  file.directory:
    - user: root
    - group: root
    - require:
      - pkg: nginx

/etc/nginx/sites-enabled/default:
  file.absent


# default site
{% set cert_path = "/etc/letsencrypt/live/{cn}/fullchain.pem".format(cn=grains['fqdn']) %}
{% set key_path = "/etc/letsencrypt/live/{cn}/privkey.pem".format(cn=grains['fqdn']) %}

{% set cert_exists = salt['file.file_exists'](cert_path) %}

/srv/www/{{ grains['fqdn'] }}/htdocs:
  file.directory:
    - user: www-data
    - group: www-data
    - makedirs: True

/var/log/nginx/{{ grains['fqdn'] }}:
  file.directory:
    - user: www-data
    - group: adm

/etc/nginx/sites-available/{{ grains['fqdn'] }}.conf:
  file.managed:
    - source:
{% if cert_exists %}
      - salt://nginx/files/sites/{{ grains['fqdn'] }}.conf.j2
      - salt://nginx/files/sites/default-https.conf.j2
{% else %}
      - salt://nginx/files/sites/default-http.conf.j2
{% endif %}
    - user: root
    - group: root
    - mode: 644
    - template: jinja
    - context:
      hostname: '{{ grains.fqdn }}'
      hostnames: '{{ grains.fqdn }}'
      ssl_cert_exists: {{ cert_exists }}
      ssl_cert_path: "{{ cert_path }}"
      ssl_key_path: "{{ key_path }}"
      access_log: "/var/log/nginx/{{ grains.fqdn }}/access.log"
      error_log: "/var/log/nginx/{{ grains.fqdn }}/error.log"
    - require:
      - pkg: nginx

/etc/nginx/sites-enabled/{{ grains['fqdn'] }}.conf:
  file.symlink:
    - target: /etc/nginx/sites-available/{{ grains['fqdn'] }}.conf
    - require:
      - file: /etc/nginx/sites-available/{{ grains['fqdn'] }}.conf

/etc/nginx/snippets/gzip.conf:
   file.managed:
    - user: root
    - group: root
    - mode: 644
    - source: salt://nginx/files/snippets/gzip.conf
    - require:
      - file: /etc/nginx/snippets
      - pkg: nginx

/etc/nginx/conf.d/hashes_size.conf:
  file.managed:
    - source: salt://nginx/files/conf.d/hashes_size.conf
    - user: root
    - group: root
    - mode: 644
    - require:
      - pkg: nginx

nginx_config_cleanup:
  file.line:
    - name: /etc/nginx/nginx.conf
    - mode: delete
    - match: server_names_hash_bucket_size

/etc/logrotate.d/nginx-custom:
   file.managed:
    - user: root
    - group: root
    - mode: 644
    - source: salt://nginx/files/logrotate
    - require:
      - pkg: nginx

# firewall rule
/etc/ferm/conf.d/40-nginx.conf:
  file.managed:
    - source: salt://nginx/files/ferm.conf
    - user: root
    - group: root
    - mode: 644
    - require:
      - pkg: ferm

include:
  - nginx.metrics
{% if 'letsencrypt' in pillar %}
  - common.dhparam
  - nginx.letsencrypt
{% endif %}
  - nginx.vhosts
{% if grains.id == 'www2.aixit.off.de.ffffm.net' %}
  - nginx.sites.net.freifunk.ffm.tiles
{% endif %}
{% if grains.id == 'www3.aixit.off.de.ffffm.net' %}
  - nginx.sites.net.ffffm.services.firmware
{% endif %}
