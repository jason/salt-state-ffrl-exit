{% for name, vhost in pillar.get('nginx', {}).get('vhosts', {}).iteritems() %}

{% set cert_path = "/etc/letsencrypt/live/{cn}/fullchain.pem".format(cn=vhost.ssl.common_name) %}
{% set key_path = "/etc/letsencrypt/live/{cn}/privkey.pem".format(cn=vhost.ssl.common_name) %}

{% set cert_exists = salt['file.file_exists'](cert_path) %}

/etc/nginx/sites-available/{{ name }}.conf:
  file.managed:
    - source: 
{%- if cert_exists %}
      - salt://nginx/files/sites/{{ name }}.conf.j2
      - salt://nginx/files/sites/default-https.conf.j2
{%- else %}
{%- if not vhost.ssl.required %}
      - salt://nginx/files/sites/{{ name }}.conf.j2
{%- else %}
      - salt://nginx/files/sites/default-http.conf.j2
{%- endif %}
{%- endif %}
    - user: root
    - group: root
    - mode: 644
    - template: jinja
    - context:
      hostname: '{{ name }}'
      hostnames: '{{ vhost.hostnames|join(' ') }}'
      ssl_cert_exists: {{ cert_exists }}
      ssl_cert_path: "{{ cert_path }}"
      ssl_key_path: "{{ key_path }}"
      access_log: "/var/log/nginx/{{ name }}/access.log"
      error_log: "/var/log/nginx/{{ name }}/error.log"
    - require:
      - pkg: nginx

/etc/nginx/sites-enabled/{{ name }}.conf:
{% if not vhost.get('disabled', False) %}
  file.symlink:
    - target: /etc/nginx/sites-available/{{ name }}.conf
{% else %}
  file.absent
{% endif %}

/srv/www/{{ name }}:
  file.directory:
    - user: www-data
    - group: www-data
    - makedirs: True

/var/log/nginx/{{ name }}:
  file.directory:
    - user: www-data
    - group: adm

{% endfor %}
