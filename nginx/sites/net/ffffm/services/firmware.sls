firmware:
  user.present:
    - shell: /bin/sh

/home/firmware/.ssh/authorized_keys:
  file.managed:
    - makedirs: True
    - user: firmware
    - group: firmware
    - mode: 600
    - require:
      - user: firmware
    - contents: |
      {%- for key in pillar['firmware']['keys'] %}
        {{ key }}
      {%- endfor %}

/home/firmware/gluon-firmware-wizard:
  git.latest:
    - name: https://github.com/freifunk-darmstadt/gluon-firmware-wizard.git
    - target: /home/firmware/gluon-firmware-wizard

/home/firmware/gluon-firmware-wizard/config.js:
  file.managed:
    - source: salt://nginx/files/firmware.services.ffffm.net/config.js
    - user: firmware
    - group: firmware
    - mode: 644
    - require:
      - git: /home/firmware/gluon-firmware-wizard

/home/firmware/images/:
  file.directory:
    - user: firmware
    - group: firmware
    - require:
      - user: firmware

/home/firmware/modules/:
  file.directory:
    - user: firmware
    - group: firmware
    - require:
      - user: firmware

/srv/www/firmware.services.ffffm.net/htdocs:
  file.symlink:
    - target: /home/firmware/gluon-firmware-wizard
    - require:
      - git: /home/firmware/gluon-firmware-wizard
