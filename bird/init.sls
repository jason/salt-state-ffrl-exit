include:
  - ferm
  - bird.exporter
{%- if 'gateway' in pillar.get('roles', [])  %}
  - bird.radv
  - bird.domains
{%- endif %}

bird:
{%- if grains.osfinger == 'Debian-8' %}
  pkgrepo.managed:
    - humanname: bird
    - name: deb http://bird.network.cz/debian/ jessie main
    - key_url: salt://bird/files/apt.key
    - file: /etc/apt/sources.list.d/bird.list
    - require_in:
      - pkg: bird
{%- else %}
  pkgrepo.absent:
    - name: deb http://bird.network.cz/debian/ jessie main
    - key_url: salt://bird/files/apt.key
{%- endif %}
  pkg.installed:
    - pkgs:
      - bird
  service.running:
    - enable: True
    - reload: True
    - watch:
      - file: /etc/bird/bird.conf
      - file: /etc/bird/bird.d/*

bird6:
  service.running:
    - enable: True
    - reload: True
    - watch:
      - file: /etc/bird/bird6.conf
      - file: /etc/bird/bird6.d/*

/etc/ferm/conf.d/40-bird.conf:
  file.managed:
    - source: salt://bird/files/ferm.conf.j2
    - template: jinja
    - user: root
    - group: root
    - mode: 644
    - require:
      - file: /etc/ferm/conf.d


/etc/bird/bird.conf:
  file.managed:
    - user: root
    - group: root
    - mode: 644
    - require:
      - pkg: bird
      - file: /etc/bird/bird.d
    - contents: |
        include "/etc/bird/bird.d/*.conf";

/etc/bird/bird6.conf:
  file.managed:
    - user: root
    - group: root
    - mode: 644
    - require:
      - pkg: bird
      - file: /etc/bird/bird6.d
    - contents: |
        include "/etc/bird/bird6.d/*.conf";

/etc/bird/bird.d:
  file.directory:
    - user: root
    - group: root
    - require:
      - pkg: bird

/etc/bird/bird6.d:
  file.directory:
    - user: root
    - group: root
    - require:
      - pkg: bird

/etc/bird/bird.d/10-local.conf:
  file.managed:
    - source: salt://bird/files/local.conf.j2
    - template: jinja
    - user: root
    - group: root
    - mode: 644
    - context:
        version: 'v4'
    - require:
      - pkg: bird
      - file: /etc/bird/bird.d

/etc/bird/bird6.d/10-local.conf:
  file.managed:
    - source: salt://bird/files/local.conf.j2
    - template: jinja
    - user: root
    - group: root
    - mode: 644
    - context:
        version: 'v6'
    - require:
      - pkg: bird
      - file: /etc/bird/bird6.d


/etc/bird/bird.d/12-announce.conf:
  file.managed:
    - source: salt://bird/files/announce.conf.j2
    - template: jinja
    - user: root
    - group: root
    - mode: 644
    - context:
        type: 'bgp4'
    - require:
      - pkg: bird
      - file: /etc/bird/bird.d

/etc/bird/bird6.d/12-announce.conf:
  file.managed:
    - source: salt://bird/files/announce.conf.j2
    - template: jinja
    - user: root
    - group: root
    - mode: 644
    - context:
        type: 'bgp6'
    - require:
      - pkg: bird
      - file: /etc/bird/bird6.d

/etc/bird/bird.d/900-kernel.conf:
  file.managed:
    - source: salt://bird/files/kernel.conf.j2
    - template: jinja
    - user: root
    - group: root
    - mode: 644
    - context:
        ip_version: 4
        kernel_table: main
        pipe_export_filter: {{ pillar['pipe_main_export_filter'] }}
    - require:
      - pkg: bird
      - file: /etc/bird/bird.d

/etc/bird/bird6.d/900-kernel.conf:
  file.managed:
    - source: salt://bird/files/kernel.conf.j2
    - template: jinja
    - user: root
    - group: root
    - mode: 644
    - context:
        ip_version: 6
        kernel_table: main
        pipe_export_filter: {{ pillar['pipe_main_export_filter'] }}
    - require:
      - pkg: bird
      - file: /etc/bird/bird6.d

{%- if 'babel' in pillar.get('roles', []) %}
/etc/bird/bird6.d/910-kernel.conf:
  file.managed:
    - source: salt://bird/files/kernel.conf.j2
    - template: jinja
    - user: root
    - group: root
    - mode: 644
    - context:
        ip_version: 6
        kernel_table: 10
        pipe_export_filter: internal_default
    - require:
      - pkg: bird
      - file: /etc/bird/bird6.d
{%- if 'gateway' in pillar.get('roles', []) %}
/etc/bird/bird6.d/912-kernel.conf:
  file.managed:
    - source: salt://bird/files/kernel.conf.j2
    - template: jinja
    - user: root
    - group: root
    - mode: 644
    - context:
        ip_version: 6
        kernel_table: 12
        pipe_export_filter: internal_default
    - require:
      - pkg: bird
      - file: /etc/bird/bird6.d
{%- endif %}
{%- endif %}
