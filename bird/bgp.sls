/etc/bird/bird.d/20-common-policy.conf:
  file.managed:
    - source: salt://bird/files/common-policy.conf
    - user: root
    - group: root
    - mode: 644
    - require:
      - pkg: bird
      - file: /etc/bird/bird.d

/etc/bird/bird6.d/20-common-policy.conf:
  file.managed:
    - source: salt://bird/files/common-policy.conf
    - user: root
    - group: root
    - mode: 644
    - require:
      - pkg: bird
      - file: /etc/bird/bird6.d


/etc/bird/bird.d/21-routing-policy.conf:
  file.managed:
    - source: salt://bird/files/routing-policy4.conf
    - user: root
    - group: root
    - mode: 644
    - require:
      - pkg: bird
      - file: /etc/bird/bird.d

/etc/bird/bird6.d/21-routing-policy6.conf:
  file.managed:
    - source: salt://bird/files/routing-policy6.conf
    - user: root
    - group: root
    - mode: 644
    - require:
      - pkg: bird
      - file: /etc/bird/bird6.d


/etc/bird/bird.d/22-templates.conf:
  file.managed:
    - source: salt://bird/files/templates4.conf
    - user: root
    - group: root
    - mode: 644
    - require:
      - pkg: bird
      - file: /etc/bird/bird.d

/etc/bird/bird6.d/22-templates.conf:
  file.managed:
    - source: salt://bird/files/templates6.conf
    - user: root
    - group: root
    - mode: 644
    - require:
      - pkg: bird
      - file: /etc/bird/bird6.d

/etc/bird/bird.d/23-bgp-route-reflector.conf:
  file.managed:
    - source: salt://bird/files/bgp-route-reflector.conf
    - user: root
    - group: root
    - mode: 644
    - require:
      - pkg: bird
      - file: /etc/bird/bird.d

/etc/bird/bird6.d/23-bgp-route-reflector.conf:
  file.managed:
    - source: salt://bird/files/bgp-route-reflector.conf
    - user: root
    - group: root
    - mode: 644
    - require:
      - pkg: bird
      - file: /etc/bird/bird6.d

/etc/bird/bird.d/30-peers.conf:
  file.managed:
    - source: salt://bird/files/peering.conf.j2
    - template: jinja
    - user: root
    - group: root
    - mode: 644
    - context:
        type: 'bgp4'
    - require:
      - pkg: bird
      - file: /etc/bird/bird.d

/etc/bird/bird6.d/30-peers.conf:
  file.managed:
    - source: salt://bird/files/peering.conf.j2
    - template: jinja
    - user: root
    - group: root
    - mode: 644
    - context:
        type: 'bgp6'
    - require:
      - pkg: bird
      - file: /etc/bird/bird6.d
