{%- set gopath = pillar.get('golang:gopath', '/usr/local/go') %}

include:
  - golang

bird-exporter:
  git.latest:
    - name: https://github.com/czerwonk/bird_exporter
    - target: {{ gopath }}/src/github.com/czerwonk/bird_exporter
    - require:
      - pkg: git
  cmd.run:
    - cwd: {{ gopath }}/src/github.com/czerwonk/bird_exporter
    - name: go get -v -u github.com/czerwonk/bird_exporter
    - env:
        GOPATH: {{ pillar.get('golang:gopath', '/usr/local/go') }}
    - require:
      - pkg: golang
      - pkg: git
      - git: bird-exporter
    - onchanges:
      - git: bird-exporter
  service.running:
    - enable: True
    - require:
      - file: /etc/systemd/system/bird-exporter.service
    - watch:
      - file: /etc/systemd/system/bird-exporter.service
      - cmd: bird-exporter

/etc/systemd/system/bird-exporter.service:
  file.managed:
    - source: salt://bird/files/bird-exporter.service.j2
    - user: root
    - group: root
    - mode: 644
    - template: jinja

/etc/ferm/conf.d/40-bird-exporter.conf:
  file.managed:
    - source: salt://bird/files/ferm-bird-exporter.conf.j2
    - user: root
    - group: root
    - mode: 644
    - template: jinja
    - require:
      - file: /etc/ferm/conf.d

prometheus_bird_export:
  grains.present:
    - value: {{ grains.nodename }}:9324
