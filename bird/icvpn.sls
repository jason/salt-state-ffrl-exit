/etc/bird/bird.d/50-icvpn.conf:
  file.managed:
    - source: salt://bird/files/icvpn4.conf.j2
    - template: jinja
    - user: root
    - group: root
    - mode: 644
    - require:
      - pkg: bird
      - file: /etc/bird/bird.d

/etc/bird/bird6.d/50-icvpn.conf:
  file.managed:
    - source: salt://bird/files/icvpn6.conf.j2
    - template: jinja
    - user: root
    - group: root
    - mode: 644
    - require:
      - pkg: bird
      - file: /etc/bird/bird6.d

icvpn-mkbgp:
  cmd.run:
    - name: /usr/local/sbin/icvpn-mkbgp
    - require:
      - file: /usr/local/sbin/icvpn-mkbgp
      - git: icvpn-meta
      - git: icvpn-scripts
      - pkg: icvpn-dependencies
    - onchanges:
      - git: icvpn-meta
      - git: icvpn-scripts

/etc/bird/bird.d/60-dn42-peer.conf:
  file.managed:
    - source: salt://bird/files/dn42-peer4.conf.j2
    - template: jinja
    - user: root
    - group: root
    - mode: 644
    - require:
      - pkg: bird
      - file: /etc/bird/bird.d

/etc/bird/bird6.d/60-dn42-peer.conf:
  file.managed:
    - source: salt://bird/files/dn42-peer6.conf.j2
    - template: jinja
    - user: root
    - group: root
    - mode: 644
    - require:
      - pkg: bird
      - file: /etc/bird/bird.d
