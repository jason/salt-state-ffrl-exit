/etc/bird/bird.d/39-direct.conf:
  file.managed:
    - source: salt://bird/files/gateway/direct.conf.j2
    - template: jinja
    - user: root
    - group: root
    - mode: 644
    - require:
      - pkg: bird
      - file: /etc/bird/bird.d

/etc/bird/bird6.d/39-direct.conf:
  file.managed:
    - source: salt://bird/files/gateway/direct.conf.j2
    - template: jinja
    - user: root
    - group: root
    - mode: 644
    - require:
      - pkg: bird
      - file: /etc/bird/bird6.d
