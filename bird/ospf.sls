/etc/bird/bird.d/29-ospf.conf:
  file.managed:
    - source: salt://bird/files/ospf.conf.j2
    - template: jinja
    - user: root
    - group: root
    - mode: 644
    - context:
        type: 'ospf4'
    - require:
      - pkg: bird
      - file: /etc/bird/bird.d

/etc/bird/bird6.d/29-ospf.conf:
  file.managed:
    - source: salt://bird/files/ospf.conf.j2
    - template: jinja
    - user: root
    - group: root
    - mode: 644
    - context:
        type: 'ospf6'
    - require:
      - pkg: bird
      - file: /etc/bird/bird6.d

/etc/bird/bird.d/20-ospf.conf:
  file.absent

/etc/bird/bird6.d/20-ospf.conf:
  file.absent
