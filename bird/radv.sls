{% for domain in pillar.get('domains', {}) %}
{% set instance = pillar['domains'][domain] %}

/etc/bird/bird6.d/50-radv-{{ domain }}.conf:
  file.managed:
    - source: salt://bird/files/gateway/radv.conf.j2
    - user: root
    - grup: root
    - mode: 644
    - template: jinja
    - context:
        domain: {{ domain }}
    - require:
       - file: /etc/bird/bird6.d

{% endfor %}
