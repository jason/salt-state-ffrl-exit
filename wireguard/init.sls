include:
  - apt.repository.sid

wireguard:
  pkg.installed:
    - pkgs:
      - wireguard-dkms
      - wireguard-tools
    - fromrepo: sid
    - require:
      - pkgrepo: sid
      - pkg: wireguard-dependencies

wirequard-apt-pin:
  file.accumulated:
    - name: apt.sid.pinning_exceptions
    - filename: /etc/apt/preferences.d/sid-pinning
    - text: wirequard*
    - require_in:
      - file: /etc/apt/preferences.d/sid-pinning

wireguard-dependencies:
  pkg.installed:
    - pkgs:
      - linux-headers-amd64

wireguard-kmod:
  kmod.present:
    - name: wireguard
    - persist: True

/etc/wg:
  file.directory
