{%- for peer in pillar.dn42.get('peers', []) if peer.tunnel.type == 'wireguard' %}

{% if peer.name == "intern" %}
{% set iface_prefix = "tun" %}
{% else %}
{% set iface_prefix = "dn42" %}
{% endif %}

/etc/wg/{{ iface_prefix }}_{{ peer.name }}.conf:
  file.managed:
    - require:
      - file: /etc/wg
    - source: salt://wireguard/files/wg.conf.j2
    - template: jinja
    - context:
      peer: {{ peer }}
{%- endfor %}

/etc/network/interfaces.d/dn42-wireguard:
  file.managed:
    - template: jinja
    - require:
      - pkg: ifupdown2
      - pkg: wireguard
    - source: salt://wireguard/files/dn42-interfaces-wireguard.j2

ifreload-wg-dn42:
  cmd.run:
    - name: /sbin/ifreload -af
    - onchanges:
      - file: /etc/network/interfaces.d/dn42-wireguard

/etc/ferm/conf.d/50-dn42-wireguard.conf:
  file.managed:
    - source: salt://wireguard/files/dn42-ferm.conf.j2
    - template: jinja
    - require:
      - file: /etc/ferm/conf.d
