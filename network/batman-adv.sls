#
# runtime settings
#

{% for domain in pillar.get('domains', {}) %}
{% set gw_mode = pillar['domains'][domain]['batman-adv'].get('gw_mode', {}).get('enabled', False) %}
{% set dat = pillar['domains'][domain]['batman-adv'].get('features', {}).get('dat', True) %}
{% set mm_mode = pillar['domains'][domain]['batman-adv'].get('features', {}).get('mm', False) %}

# gateway mode
{% if gw_mode  %}
batctl_{{ domain }}_gw_mode:
  cmd.run:
    - name: batctl -m {{ domain }}-bat gw server {{ pillar['domains'][domain]['batman-adv']['gw_mode']['uplink'] }}/{{ pillar['domains'][domain]['batman-adv']['gw_mode']['downlink'] }}
    - unless: "[ \"$(batctl -m {{ domain }}-bat gw | awk '{ print $1 }')\" = \"server\" ]"
{% else %}
batctl_{{ domain }}_gw_mode:
  cmd.run:
    - name: batctl -m {{ domain }}-bat gw off
    - unless: "[ \"$(batctl -m {{ domain }}-bat gw | awk '{ print $1 }')\" = \"off\" ]"
{% endif %}

{% if dat  %}
batctl_{{ domain }}_dat:
  cmd.run:
    - name: batctl -m {{ domain }}-bat dat 1
    - unless: "[ \"$(batctl -m {{ domain }}-bat dat)\" = \"enabled\" ]"
{% else %}
batctl_{{ domain }}_dat:
  cmd.run:
    - name: batctl -m {{ domain }}-bat dat 0
    - unless: "[ \"$(batctl -m {{ domain }}-bat dat)\" = \"disabled\" ]"
{% endif %}

{%- if pillar['domains'][domain]['mesh_proto'] == 'batman-adv' %}
# multicast mode
{% if mm_mode %}
batctl_{{ domain }}_mm_mode:
  cmd.run:
    - name: batctl -m {{ domain }}-bat mm 1
    - unless: "[ \"$(batctl -m {{ domain }}-bat mm)\" = \"enabled\" ]"
clientbr_{{ domain }}_multicast_snooping:
  cmd.run:
    - name: echo 1 > /sys/class/net/{{ domain }}-br/bridge/multicast_snooping
    - unless: "[ \"$(cat /sys/class/net/{{ domain }}-br/bridge/multicast_snooping)\" = \"1\" ]"
{% else %}
batctl_{{ domain }}_mm_mode:
  cmd.run:
    - name: batctl -m {{ domain }}-bat mm 0
    - unless: "[ \"$(batctl -m {{ domain }}-bat mm)\" = \"disabled\" ]"
clientbr_{{ domain }}_multicast_snooping:
  cmd.run:
    - name: echo 0 > /sys/class/net/{{ domain }}-br/bridge/multicast_snooping
    - unless: "[ \"$(cat /sys/class/net/{{ domain }}-br/bridge/multicast_snooping)\" = \"0\" ]"

{% endif %}
{% endif %}

{% endfor %}
