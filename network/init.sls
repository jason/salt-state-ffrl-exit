#
## Networking
#

iproute2:
  pkg.installed

ifupdown2:
  pkg.installed

python-ipcalc:
  pkg.installed

network-pkg:
  pkg.installed:
    - pkgs:
      - bridge-utils
      - vlan
      - tcpdump
      - vnstat
      - host
      - ipv6calc


# ifupdown2 configuration
/etc/network/ifupdown2/ifupdown2.conf:
  file.managed:
    - source: salt://network/files/ifupdown2.conf
    - require:
      - pkg: ifupdown2
      - pkg: python-ipcalc


# Reload interface configuration if neccessary
ifreload:
  cmd.wait:
    - name: /sbin/ifreload -af
#    - watch:
#      - file: /etc/network/interfaces
    - require:
      - file: /etc/network/ifupdown2/ifupdown2.conf
