{% for domain in pillar.get('domains', {}).keys() %}

/etc/network/interfaces.d/domain-{{ domain }}:
  file.managed:
    - source: salt://network/files/interfaces-domain.j2
    - mode: 644
    - user: root
    - group: root
    - template: jinja
    - context:
        domain: {{ domain }}

{% if 'gateway' in pillar.get('roles', []) %}
/etc/ferm/conf.d/20-domain-{{ domain }}.conf:
  file.managed:
    - source: salt://network/files/ferm-domain-batman.conf.j2
    - user: root
    - group: root
    - mode: 644
    - template: jinja
    - context:
        domain: {{ domain }}
        routing: {{ pillar['routing'] }}
{% endif %}

ifup-{{ domain }}-br:
  cmd.run:
    - name: /sbin/ifup {{ domain }}-br
    - unless: /sbin/ip link show dev {{ domain }}-br
    - require:
      - cmd: ifreload
{% endfor %}
