/etc/iproute2/rt_tables.d/babel.conf:
  file.managed:
  - contents: |
      10    netz
      11    l3roamd
      12    babeld

/etc/systemd/system/network-online.target.wants/iprules.service:
  file.managed:
    - source: salt://network/files/babel-rules.service
    - user: root
    - group: root
    - mode: 0644

iprules:
  service.enabled:
    - require:
      - file: /etc/systemd/system/network-online.target.wants/iprules.service
    - watch:
      - file: /etc/systemd/system/network-online.target.wants/iprules.service
