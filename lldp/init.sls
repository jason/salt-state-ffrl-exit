{%- set interfaces = [] -%}
{%- for name, interface in pillar.ifaces.items() if 'lldp' in interface -%}
  {%- do interfaces.append(name) -%}
{%- endfor -%}
{%- if interfaces|length > 0 -%}
lldpd:
  pkg.installed: []
  service.running:
    - enable: True
    - reload: False
    - watch:
      - pkg: lldpd
      - file: /etc/default/lldpd

/etc/default/lldpd:
  file.managed:
    - user: root
    - group: root
    - mode: 644
    - template: jinja
    - source: salt://lldp/files/default-lldp.j2
{%- else %}
purge-lldpd:
  pkg.purged:
    - pkgs:
      - lldpd

purge-lldpd-default:
  file.absent:
    - name: /etc/default/lldpd
{% endif %}
