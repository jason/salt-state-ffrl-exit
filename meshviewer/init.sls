{% set map_domain = "map.ffm.freifunk.net" %}

include:
  - apt.repository.nodejs
  - apt.repository.yarn
  - common.packages

meshviewer:
  user.present:
    - shell: /usr/sbin/nologin

/home/meshviewer/meshviewer.git:
  git.latest:
    - name: https://github.com/ffrgb/meshviewer.git
    - target: /home/meshviewer/meshviewer.git
    - user: meshviewer
    - force_fetch: True
    - force_reset: True
    - refspec_branch: develop
    - require:
      - pkg: git
      - pkg: nodejs
      - user: meshviewer

meshviewer_remove_build:
  cmd.run:
    - onchanges:
      - git: /home/meshviewer/meshviewer.git
      - pkg: nodejs
    - watch:
      - git: /home/meshviewer/meshviewer.git
      - pkg: nodejs
    - cwd: /home/meshviewer/meshviewer.git
    - name: rm -rf build

meshviewer_yarn_install:
  cmd.run:
    - onchanges:
      - cmd: meshviewer_remove_build
    - require:
      - pkg: yarn
      - cmd: meshviewer_remove_build
    - cwd: /home/meshviewer/meshviewer.git
    - user: meshviewer
    - name: yarn

meshviewer_install_config:
  file.managed:
    - name: /home/meshviewer/meshviewer.git/config.js
    - user: www-data
    - group: www-data
    - source: salt://meshviewer/files/config.js.j2
    - template: jinja
    - require:
      - git: /home/meshviewer/meshviewer.git
    - onchanges:
      - git: /home/meshviewer/meshviewer.git

meshviewer_gulp:
  cmd.run:
    - onchanges:
      - cmd: meshviewer_yarn_install
      - file: meshviewer_install_config

    - require:
       - cmd: meshviewer_yarn_install
       - git: /home/meshviewer/meshviewer.git

    - cwd: /home/meshviewer/meshviewer.git
    - user: meshviewer
    - name: yarn run gulp

meshviewer_empty_srv_www:
  file.absent:
    - name: /srv/www/{{ map_domain }}/htdocs
    - require:
       - file: /srv/www/{{ map_domain }}
       - cmd: meshviewer_gulp
    - onchanges:
       - cmd: meshviewer_gulp

meshviewer_create_srv_www:
  file.directory:
    - user: www-data
    - group: www-data
    - name: /srv/www/{{ map_domain }}/htdocs
    - require:
       - file: meshviewer_empty_srv_www
    - onchanges:
       - file: meshviewer_empty_srv_www

meshviewer_copy_to_srv_www:
  cmd.run:
    - name: cp -ar /home/meshviewer/meshviewer.git/build/* /srv/www/{{ map_domain }}/htdocs/. && chown www-data:www-data -R /srv/www/{{ map_domain }}/htdocs/
    - require:
       - file: meshviewer_create_srv_www
    - onchanges:
       - file: meshviewer_create_srv_www


# the repository is always in a dirty state forcing hard-resets
# even though only local changes occured. Therefore let's reset
# our local changes after a successful build
meshviewer_clean_repo:
  cmd.run:
    - name: git -C /home/meshviewer/meshviewer.git checkout config.js
    - onchanges:
      - cmd: meshviewer_copy_to_srv_www
