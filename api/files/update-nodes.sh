#!/bin/bash

set -e

if [[ $# -ne 1 ]]; then
    >&2 echo "`basename "$0"` 'path/to/api.json'"
    exit
fi

apiFile=$1
nodesFiles=$(jq -r '.nodeMaps[]|select(.mapType=="structural").url' "${apiFile}")
currentDate=$(date -u '+%Y-%m-%dT%T.%NZ')

totalNodesCount=0
for nodesFile in $nodesFiles; do
    nodesContent=$(curl -s --fail "${nodesFile}")
    nodesCount=$(echo "${nodesContent}" | jq '[.nodes[]|select(.flags.online==true)]|length')
    totalNodesCount=$(("${totalNodesCount}"+"${nodesCount}"))
done

apiFileContent=$(jq ".state.nodes=${totalNodesCount}|.state.lastchange=\"${currentDate}\"" "${apiFile}")

echo "${apiFileContent}" > "${apiFile}"
