{% set api_domain = "api.ffm.freifunk.net" %}
{% set api_community_json = "/var/lib/" ~ api_domain ~ "-community" %}
{% set api_update_community_command = "/home/freifunk/update-nodes.sh \"" ~ api_community_json ~ "/ff-frankfurt.json\"" %}
{% set api_git_update_command = "(cd \"" ~ api_community_json ~ "\" && git reset --hard HEAD && git pull && " ~ api_update_community_command ~ ") &> /dev/null" %}

api_create_srv_www_community_api:
  file.directory:
    - name: /srv/www/{{ api_domain }}/htdocs
    - user: www-data
    - group: www-data

api_update_community_repository:
  git.latest:
    - name: https://github.com/freifunk-ffm/directory.api.freifunk.net.git
    - target: {{ api_community_json }}
    - force_fetch: True
    - force_reset: True
    - require:
      - pkg: git
  file.directory:
    - name: {{ api_community_json }}
    - user: freifunk
    - group: www-data
    - file_mode: 744
    - dir_mode: 755
    - recurse:
      - user
      - group
      - mode
    - require:
      - user: freifunk

api_symlink_community_json:
  file.symlink:
    - name: /srv/www/{{ api_domain }}/htdocs/ff-frankfurt.json
    - target: {{ api_community_json }}/ff-frankfurt.json
    - force: True
    - require:
       - git: api_update_community_repository
       - file: api_create_srv_www_community_api

api_place_community_updater_script:
  file.managed:
    - name: /home/freifunk/update-nodes.sh
    - source: salt://api/files/update-nodes.sh
    - user: freifunk
    - mode: 755
    - require:
      - user: freifunk
  pkg.installed:
    - pkgs:
      - curl
      - jq

api_update_community_crontab:
  cron.present:
    - name:  {{ api_git_update_command }}
    - identifier: api-updater
    - user: freifunk
    - minute: '*/5'
    - require:
      - user: freifunk
      - git: api_update_community_repository
      - file: api_place_community_updater_script

api_run_update_community_json:
  cmd.run:
    - name: {{ api_update_community_command }}
    - require:
      - git: api_update_community_repository
      - file: api_place_community_updater_script
