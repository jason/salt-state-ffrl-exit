include:
  - ferm

https://github.com/freifunk-darmstadt/mesh-announce.git:
  git.latest:
    - target: /opt/mesh-announce
    - force_fetch: True

/etc/systemd/system/mesh-announce@.service:
  file.managed:
    - source: salt://mesh-announce/files/mesh-announce@.service
    - user: root
    - group: root
    - mode: 644

mesh-announce_deps:
  pkg.installed:
     - pkgs:
       - ethtool
       - lsb-release
       - python3-netifaces

/etc/mesh-announce:
  file.directory

{% for domain in pillar.get('domains', {}) %}
/etc/mesh-announce/{{ domain }}:
  file.managed:
    - source: salt://mesh-announce/files/domain.j2
    - template: jinja
    - user: root
    - group: root
    - context:
      domain: {{ domain }}

mesh-announce@{{ domain }}:
  service.running:
    - enable: True
    - require:
      - git: https://github.com/freifunk-darmstadt/mesh-announce.git
      - file: /etc/systemd/system/mesh-announce@.service
      - file: /etc/mesh-announce/{{ domain }}
      - pkg: mesh-announce_deps
    - watch:
      - git: https://github.com/freifunk-darmstadt/mesh-announce.git
      - file: /etc/systemd/system/mesh-announce@.service
      - file: /etc/mesh-announce/{{ domain }}

/etc/ferm/conf.d/50-mesh-announce-{{ domain }}.conf:
  file.managed:
    - source: salt://mesh-announce/files/ferm.conf.j2
    - template: jinja
    - user: root
    - group: root
    - mode: 644
    - context:
      domain: {{ domain }}
    - require:
      - pkg: ferm
{% endfor %}
