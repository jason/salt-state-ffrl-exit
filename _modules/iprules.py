def gen_ip_rule(ruleset, action):
    rule = []

    if 'netns' in ruleset:
        rule.append('/sbin/ip/ netns exec {}'.format(ruleset['netns']))

    rule.append('/sbin/ip -{} rule'.format(ruleset['proto']))
    rule.append(action)
    rule.append('priority {}'.format(ruleset['priority']))
    if 'not' in ruleset:
        rule.append('not')
    if 'from' in ruleset:
        rule.append('from {}'.format(ruleset['from']))
    if 'to' in ruleset:
        rule.append('from {}'.format(ruleset['to']))
    if 'tos' in ruleset:
        rule.append('from {}'.format(ruleset['tos']))
    if 'fwmark' in ruleset:
        rule.append('from {}'.format(ruleset['fwmark']))
    if 'iif' in ruleset:
        rule.append('from {}'.format(ruleset['iif']))
    if 'oif' in ruleset:
        rule.append('from {}'.format(ruleset['oif']))
    rule.append('lookup {}'.format(ruleset['table']))

    return ' '.join(rule)
