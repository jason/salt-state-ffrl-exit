def prometheus_targets_for_grain(key):
    values = __salt__['mine.get']('*', 'grains.items', expr_form='glob').values()
    return [ remote_grains[key] for remote_grains in sorted(values) if key in remote_grains ]
