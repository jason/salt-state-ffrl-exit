import logging
import socket

LOG = logging.getLogger(__name__)

def resolve(hostname):
    try:
         return {x[4][0] for x in socket.getaddrinfo(hostname, 80)}
    except:
        return ()


