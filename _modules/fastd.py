import logging

# compose mac for fastd interface
def mac(port, host_id):
    return ":".join(['02', 'FF', str(port)[:2], str(port)[2:], host_id, '03'])

# enumerate fastd instance names for fastd exporter
def all_instances():
    tmp = []
    for domain in __salt__['pillar.get']('domains'):
        instances = __salt__['pillar.get']('domains:{0}:fastd:instances'.format(domain), [])
        tmp.extend(["{0}{1}".format(domain, instance['mtu']) for instance in instances])
    return tmp
def all_instances_name():
    tmp = []
    for domain in __salt__['pillar.get']('domains'):
        instances = __salt__['pillar.get']('domains:{0}:fastd:instances'.format(domain), [])
        tmp.extend(["{0}-vpn-{1}".format(domain, instance['mtu']) for instance in instances])
    return tmp

#  enumerate fastd interface names for mesh-announce
def ifnames_for_domain(domain):
    instances = __salt__['pillar.get']('domains:{}:fastd:instances'.format(domain), [])
    return ["{0}-vpn-{1}".format(domain, instance['mtu']) for instance in instances]

# enumerate ports to open up in ferm
def ports_for_domain(domain):
    instances = __salt__['pillar.get']('domains:{}:fastd:instances'.format(domain), [])
    return [str(instance['port']) for instance in instances]
