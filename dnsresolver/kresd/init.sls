include:
  - ferm

knot-resolver:
  pkg.installed:
{% if grains['osfinger'] == "Debian-9" %}
    - fromrepo: stretch-backports
{% endif %}
    - pkgs:
      - knot-resolver
      - dns-root-data
      - knot-dnsutils
      - knot-host
      - knot-resolver-module-http
  service.running:
    - name: knot-resolver
    - enable: True
    - reload: True
    - watch:
      - pkg: knot-resolver
      - file: /etc/knot-resolver/kresd.conf

/etc/knot-resolver/kresd.conf:
  file.managed:
    - source: salt://dnsresolver/kresd/files/kresd.conf.j2
    - user: root
    - group: root
    - mode: 644
    - template: jinja

/etc/network/interfaces.d/dns-anycast-lo:
  file.managed:
    - source: salt://dnsresolver/files/anycast-lo.conf.j2
    - mode: 644
    - user: root
    - group: root
    - template: jinja

/etc/ferm/conf.d/40-kresd.conf:
  file.managed:
    - source:
      - salt://dnsresolver/kresd/files/ferm.conf.j2
      - salt://dnsresolver/files/ferm-default.conf.j2
    - user: root
    - group: root
    - mode: 644
    - template: jinja
    - require:
      - file: /etc/ferm/conf.d
