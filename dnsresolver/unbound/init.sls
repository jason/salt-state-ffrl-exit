include:
  - ferm
  - dnsresolver.unbound.exporter

unbound:
  pkg.installed:
{% if grains['osfinger'] == "Debian-8" %}
    - fromrepo: jessie-backports
{% endif %}
    - pkgs:
      - unbound
      - dns-root-data
  service.running:
    - enable: True
    - reload: True
    - watch:
      - file: /etc/unbound/unbound.conf

/etc/unbound/unbound.conf:
  file.managed:
    - source: salt://dnsresolver/unbound/files/unbound.conf.j2
    - user: root
    - group: root
    - mode: 644
    - template: jinja

/etc/unbound/unbound_server.pem:
  file.managed:
    - user: unbound
    - group: unbound

/etc/unbound/unbound_control.pem:
  file.managed:
    - user: unbound
    - group: unbound

/etc/unbound/unbound_control.key:
  file.managed:
    - user: unbound
    - group: unbound

/etc/network/interfaces.d/dns-anycast-lo:
  file.managed:
    - source: salt://dnsresolver/files/anycast-lo.conf.j2
    - mode: 644
    - user: root
    - group: root
    - template: jinja

/etc/ferm/conf.d/40-unbound.conf:
  file.managed:
    - source:
      - salt://dnsresolver/unbound/files/ferm.conf.j2
      - salt://dnsresolver/files/ferm-default.conf.j2
    - user: root
    - group: root
    - mode: 644
    - template: jinja
    - require:
      - file: /etc/ferm/conf.d
