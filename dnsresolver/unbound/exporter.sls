{%- set gopath = pillar.get('golang:gopath', '/usr/local/go') %}
{%- set gopkg = 'github.com/kumina/unbound_exporter' %}

include:
  - golang

unbound-exporter:
  git.latest:
    - name: https://{{ gopkg }}
    - target: {{ gopath }}/src/{{ gopkg }}
  cmd.run:
    - cwd: {{ gopath }}/src/{{ gopkg }}
    - name: go get -v -u {{ gopkg }}
    - env:
        GOPATH: {{ pillar.get('golang:gopath', '/usr/local/go') }}
    - require:
      - pkg: golang
      - git: unbound-exporter
    - onchanges:
      - git: unbound-exporter
  service.running:
    - enable: True
    - require:
      - file: /etc/systemd/system/unbound-exporter.service
    - watch:
      - file: /etc/systemd/system/unbound-exporter.service
      - cmd: unbound-exporter

/etc/systemd/system/unbound-exporter.service:
  file.managed:
    - source: salt://dnsresolver/unbound/files/unbound-exporter.service.j2
    - user: root
    - group: root
    - mode: 644
    - template: jinja

/etc/ferm/conf.d/40-unbound-exporter.conf:
  file.managed:
    - source: salt://dnsresolver/unbound/files/ferm-unbound-exporter.conf.j2
    - user: root
    - group: root
    - mode: 644
    - template: jinja
    - require:
      - file: /etc/ferm/conf.d

prometheus_unbound_export:
  grains.present:
    - value: {{ grains.nodename }}:9167
