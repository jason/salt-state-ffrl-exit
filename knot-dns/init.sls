{% set knot = pillar['knot-dns'] %}

include:
  - apt.repository.knot-dns
  - ferm

knot:
  pkg.installed:
    - pkgs:
      - knot
  service.running:
    - name: knot
    - enable: True
    - reload: True
    - watch:
      - pkg: knot
      - file: /etc/knot/knot.conf

/etc/knot/knot.conf:
  file.managed:
    - source: salt://knot-dns/files/knot.conf.j2
    - user: knot
    - group: knot
    - mode: 644
    - template: jinja
    - require:
      - pkg: knot

/var/lib/knot/:
  file.directory:
    - user: knot
    - group: knot
    - order: last
    - recurse:
      - user
      - group
    - require:
      - pkg: knot

/var/lib/knot/zones/:
  git.latest:
    - name: {{ knot['repository']['remote'] }}
    - branch: {{ knot['repository']['branch'] }}
    - target: /var/lib/knot/zones/
    - watch_in:
      - service: knot
    - require:
      - pkg: git
      - pkg: knot
  file.directory:
    - makedirs: True
    - user: knot
    - group: knot
    - recurse:
      - user
      - group

/etc/ferm/conf.d/40-knot.conf:
  file.managed:
    - source: salt://knot-dns/files/ferm.conf.j2
    - user: root
    - group: root
    - mode: 644
    - template: jinja
    - require:
      - file: /etc/ferm/conf.d
